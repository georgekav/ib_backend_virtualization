__author__ = 'Olivier Van Cutsem'

import json
import math
import numpy
from conf_general import PUB_MSG_TYPE
from vEntity import vEntity
from conf_virtualization import *

# KEYS FOR BMS
VE_BAT_GEN_PARAM = 'general_param'
VE_BAT_MODEL_PARAM = 'model_param'

VE_BAT_GEN_PARAM_CAPA = 'nominal_capacity'
VE_BAT_GEN_PARAM_SOC_INIT  = 'initial_soc'
VE_BAT_GEN_PARAM_CH_EFF = 'charging_efficiency'
VE_BAT_GEN_PARAM_DISCH_EFF = 'discharging_efficiency'
VE_BAT_GEN_PARAM_CYCLES = 'life_cycles'
VE_BAT_GEN_PARAM_LEAK = 'leakage_coeff'
VE_BAT_GEN_PARAM_MAX_P_CH = 'max_power_charge'
VE_BAT_GEN_PARAM_MAX_P_DISCH = 'max_power_discharge'

VE_STORAGE_SLEEP_PARAM = "sleeping_param"
VE_STORAGE_SLEEP_SLIDING_WINDOW = "sliding_window_size"
VE_STORAGE_SLEEP_SLIDING_WINDOW_WEIGHT = "sliding_window_weight"

# Entity States
CHARGE_vStorage_STATES = 'CHARGE'
DISCHARGE_vStorage_STATES = 'DISCHARGE'
FREE_AFTER_CHARGE_vStorage_STATES = 'FREE_CH'
FREE_AFTER_DISCHARGE_vStorage_STATES = 'FREE_DIS'
FULL_vStorage_STATES = 'FULL'
EMPTY_vStorage_STATES = 'EMPTY'


class vStorage(vEntity):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """

        #  Entity identification
        super(vStorage, self).__init__(pubsubsocketpair=pubsubsocketpair, param=vEparameters, ownid=vEid, entitieslogger=logger)
        self.vEid = vEid
        model = vEparameters[VE_MODEL]
        self.model_param = model[VE_BAT_MODEL_PARAM]

        # TODO: check the validity of the parameters
        ######  Battery characteristics  ######
        ####  Constants

        # Total capacity
        self.total_capa = model[VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_CAPA] * 3600  # received in Wh

        # Peak power charge qnd discharge:
        self.disch_efficiency = model[VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_DISCH_EFF]  # Ideally as a function of SOC
        self.ch_efficiency = model[VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_CH_EFF]

        self.max_power_ch = model[VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_MAX_P_CH]  # in Watt - Ideally as a function of SOC
        self.max_power_disch = model[VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_MAX_P_DISCH]  # Ideally as a function of SOC

        # Life cycles
        self.tot_life_cycles = model[VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_CYCLES]

        # X * 1/(3600 * 24 *30) = X% per month
        self.leakage = model[VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_LEAK]

        ##########  Variables #########
        # Description of the current state:
        # 'STATE': internal label to keep track of the actual state (in charge, unused, etc), and sometimes the former state
        # 'SOC': state of charge of the battery, a number between 0 and 1 that indicates the battery charge level
        # 'CYCLES': the number of discharge/charge changes the battery has experienced
        # 'LAST_T': timestamp of the last model iteration
        # 'SOC_LAST_TRANSITION': the SOC of the battery when the last change of state (charge/discharge trans) occured

        self.state = {'STATE': FREE_AFTER_CHARGE_vStorage_STATES,
                      'SOC': model[VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_SOC_INIT],
                      'P': 0,
                      'CYCLES': 0,
                      'LAST_T': self.current_time,
                      'SOC_LAST_TRANSITION': model[VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_SOC_INIT]}

        ### SLEEPING adaptation parameters
        sleep_param = vEparameters[VE_SIMU_PARAM][VE_STORAGE_SLEEP_PARAM]
        self.dt_nom = self.dt
        self.dt_window_len = sleep_param[VE_STORAGE_SLEEP_SLIDING_WINDOW]
        self.weight_past_dt = sleep_param[VE_STORAGE_SLEEP_SLIDING_WINDOW_WEIGHT]

        self.past_dt = [float(self.dt_nom)] * self.dt_window_len

        # internal way to pass variables to various functions
        self.model_change = {'NEW_DELTA_SOC': 0, 'NEW_DELTA_T': 0}

        # for "in-the-loop" purpose
        self.inter_function_variable['RECEIVED_MSG'] = None

    def _process_incoming_event(self, sender, msg):
        """ The incoming message is a command """
        self.inter_function_variable['RECEIVED_MSG'] = msg

    def _update_model_state(self):
        """
        Run an iteration of the vStorage model, in order to update the SOC
        :return: /
        """

        # Based on the current time, determine the dt of the simulation
        now = self.current_time
        delta_t = now - self.state['LAST_T']
        self.state['LAST_T'] = now
        old_soc = self.state['SOC']

        # Leakage if SoC > 0
        if self.state['SOC'] > 0:
            self.state['SOC'] -= self.state['SOC'] * self.leakage * delta_t

        if self.state['STATE'] == FULL_vStorage_STATES:  # Nothing to do
            pass
        elif self.state['STATE'] == EMPTY_vStorage_STATES:  # Nothing to do
            pass
        elif self.state['STATE'] == FREE_AFTER_CHARGE_vStorage_STATES or self.state['STATE'] == FREE_AFTER_DISCHARGE_vStorage_STATES:
            pass
        elif self.state['STATE'] == CHARGE_vStorage_STATES or self.state['STATE'] == DISCHARGE_vStorage_STATES:  # Constraints non applied and P not null
            # UPDATE THE SoC: the task of the sub-class
            self.update_soc(delta_t)
            self.state['SOC'] = max(min(self.state['SOC'], 1), 0)  # Wrap it
            self.change_accumulator['SOC'] += (self.state['SOC'] - old_soc)  # update SoC change

        # Give the opportunity to the others to work
        self.quantized_sleep()

        # Then update the LABEL state
        self.model_change['NEW_DELTA_T'] = delta_t
        self.update_state_label()

        # change of the model
        self.model_change['NEW_DELTA_SOC'] = self.state['SOC'] - old_soc

    def update_soc(self, dt):
        """
        Here is a simple integrative model. Normally this function should be overloaded
        :return:
        """
        eff_p = self.disch_efficiency
        if self.state['STATE'] == CHARGE_vStorage_STATES:
            eff_p = self.ch_efficiency

        self.state['SOC'] += eff_p * self.state['P'] * dt / (self.total_capa * 3600)

    def get_max_power(self, p):
        """
        No limitation for the power in the generic model. Normally this function should be overloaded
        :return:
        """
        return p

    def update_state_label(self):
        """
        According to the uplink command message, change the current internal state.
        That might involve to enter into a new cycle.
        :return: /
        """
        command_msg = self.inter_function_variable['RECEIVED_MSG']
        old_state = self.state['STATE']

        if self.state['SOC'] == 1:
            self.state['STATE'] = FULL_vStorage_STATES
        elif self.state['SOC'] == 0:
            self.state['STATE'] = EMPTY_vStorage_STATES

        if command_msg is not None and vEntity.isCommand(command_msg) and command_msg['PAYLOAD']['Type'] == TYPE_ACTION_NEW_P:
            old_P = self.state['P']
            new_p = command_msg['PAYLOAD']['Setting']

            if new_p == 0:  # the battery is neither discharging nor charging
                self.state['P'] = 0
                if self.state['STATE'] != FULL_vStorage_STATES and self.state['STATE'] != EMPTY_vStorage_STATES:
                    if self.state['STATE'] == CHARGE_vStorage_STATES:
                        self.state['STATE'] = FREE_AFTER_CHARGE_vStorage_STATES
                    elif self.state['STATE'] == DISCHARGE_vStorage_STATES:
                        self.state['STATE'] = FREE_AFTER_DISCHARGE_vStorage_STATES

            else:  # new non-zero value for P
                max_p = self.get_max_power(new_p)
                if new_p > 0:  # the battery has to absorb power
                    if new_p > max_p:
                        self.state['P'] = max_p
                    else:
                        self.state['P'] = new_p

                    if self.state['STATE'] != FULL_vStorage_STATES:  # If battery is full, cannot charge it any further
                        self.state['STATE'] = CHARGE_vStorage_STATES

                elif new_p < 0:  # the battery has to give power
                    if new_p < max_p:
                        self.state['P'] = max_p
                    else:
                        self.state['P'] = new_p

                    if self.state['STATE'] != EMPTY_vStorage_STATES:  # If battery is empty, cannot discharge it any further
                        self.state['STATE'] = DISCHARGE_vStorage_STATES

            self.change_accumulator['P'] += (self.state['P'] - old_P)

        if self.state['STATE'] == FULL_vStorage_STATES or self.state['STATE'] == EMPTY_vStorage_STATES:
            self.state['P'] = 0

        # Adapt the cycles amount
        if self.state['STATE'] == DISCHARGE_vStorage_STATES and (old_state == CHARGE_vStorage_STATES or old_state == FREE_AFTER_CHARGE_vStorage_STATES or old_state == FULL_vStorage_STATES)\
                or self.state['STATE'] == CHARGE_vStorage_STATES and (old_state == DISCHARGE_vStorage_STATES or old_state == FREE_AFTER_DISCHARGE_vStorage_STATES or old_state == EMPTY_vStorage_STATES):
                self.update_cycles_number()

        if old_state != self.state['STATE']:
            self.change_accumulator['STATE'] = 1

    def update_cycles_number(self):
        # TODO: validate the technique !
        self.state['CYCLES'] += abs(self.state['SOC_LAST_TRANSITION'] - self.state['SOC'])
        self.state['SOC_LAST_TRANSITION'] = self.state['SOC']

    def _create_send_event(self):
        """Abstract method of _model_setup that need to be overloaded by the children class.
        bla bla bla."""
        change_of_state = False
        for k in self.change_accumulator:
            if abs(self.change_accumulator[k]) >= self.change_threshold[k]:
                change_of_state = True
                self.change_accumulator[k] = 0

        if change_of_state or self.current_time - self.last_msg_timestamp > VE_MAX_MESSAGE_PERIOD_THRESHOLD:
            upward_event = {'P': self.state['P'], 'SOC': self.state['SOC'], 'CYCLES': self.state['CYCLES']}
            self.send_event(receiver=VMID_MANAGER_ADDR, msg=json.dumps(
                {'MSG_TYPE': EVENT_MSG_TYPE, 'PAYLOAD': {'Type': PUB_MSG_TYPE[1], 'Quantity': upward_event}}))
            ##########################

    def _update_sleep_interval(self):
        """Abstract method of _model_setup that need to be overloaded by the children class.
        bla bla bla."""
        model_var = self.model_change
        # Previous data average and then add the new one
        average_prev_data = float(sum(self.past_dt) / self.dt_window_len)
        self.past_dt = numpy.roll(self.past_dt, 1)
        self.past_dt[0] = model_var['NEW_DELTA_T']

        # New dt based only on the previous computation
        new_dt = self.dt
        if abs(model_var['NEW_DELTA_SOC']) > 0:
            new_dt = model_var['NEW_DELTA_T'] * float(self.change_threshold['SOC'] / abs(model_var['NEW_DELTA_SOC']))

        # New dt: mix between the "old average" and the new value
        k = 0.15  # weight of past data
        self.dt = self.weight_past_dt * average_prev_data + (1 - self.weight_past_dt) * new_dt
        self.dt = max(self.dt, self.dt_nom)
