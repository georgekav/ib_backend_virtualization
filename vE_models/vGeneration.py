__author__ = 'Olivier Van Cutsem'

import json

from conf_general import PUB_MSG_TYPE
from vEntity import vEntity
from conf_virtualization import *
VGEN_VSENSOR_RAW_DATA = 'raw_data_interest'
VGEN_SENSOR_KEY_QUANTITY = "P"

# Entity States
INSIDE_VGENERATION_STATES = 'FEEDING_IN'
OUTSIDE_VGENERATION_STATES = 'FEEDING_OUT'
OFF_VGENERATION_STATES = 'NO_GEN'

class vGeneration(vEntity):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """

        #  Entity identification
        super(vGeneration, self).__init__(pubsubsocketpair=pubsubsocketpair, param=vEparameters, ownid=vEid, entitieslogger=logger)
        model = vEparameters[VE_SIMU_PARAM]

        #  Variables
        self.in_sensor = {}
        for r_d in model[VGEN_VSENSOR_RAW_DATA]:
            self.in_sensor[r_d] = None

        self.state = {'STATE': OFF_VGENERATION_STATES, VGEN_SENSOR_KEY_QUANTITY: 0.0}

    def _process_incoming_event(self, sender, msg):
        """Abstract method of _model_setup that need to be overloaded by the children class.
        bla bla bla."""

        # TODO: detailed check of the msg
        if 'Type' in msg and msg['Type'] == PUB_MSG_TYPE[1]:
            for k in self.in_sensor:
                if k in msg['Quantity']:
                    self.in_sensor[k] = msg['Quantity'][k]
                    break

    def _update_model_state(self):
        """Compute the new output power according to the new values of LUM and TEMP"""

        # Aimed to be overwritten
        pass

    def _create_send_event(self):
        """Abstract method of _model_setup that need to be overloaded by the children class.
        bla bla bla."""

        if abs(self.change_accumulator[VGEN_SENSOR_KEY_QUANTITY]) >= self.change_threshold[VGEN_SENSOR_KEY_QUANTITY]:
            quant = {VGEN_SENSOR_KEY_QUANTITY: self.state[VGEN_SENSOR_KEY_QUANTITY]}
            self.send_event(receiver=VMID_MANAGER_ADDR, msg=json.dumps({'MSG_TYPE': EVENT_MSG_TYPE, 'PAYLOAD': {'Type': PUB_MSG_TYPE[1], 'Quantity': quant}}))
            self.change_accumulator[VGEN_SENSOR_KEY_QUANTITY] = 0.0
