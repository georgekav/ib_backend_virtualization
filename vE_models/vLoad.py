__author__ = 'Olivier Van Cutsem'

import json
import logging
from conf_general import PUB_MSG_TYPE
from vEntity import vEntity
from conf_virtualization import *
from load_profile import *


# KEYS FOR BMS
VLOAD_KEY_CURRENT_MODE = "CUR_MODE"
VLOAD_KEY_CURRENT_PARAM = "CUR_PARAM"
VLOAD_KEY_CURRENT_SEQUENCE_INDEX = "CUR_SEQ_ELEM"
VLOAD_KEY_LAST_MODE_CHANGE = "LAST_MODE_CHANGE"
VLOAD_KEY_MODE_DURATION = "DUR_IN_MODE"
VLOAD_KEY_MODE_OFF = "MODE_OFF"
VLOAD_KEY_STATE_START = "STATE_START"
VLOAD_KEY_STATE_END = "STATE_END"

# this key must be compatible with the BMS
VLOAD_SENSOR_KEY_QUANTITY = "P"


class vLoad(vEntity):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """
        super(vLoad, self).__init__(pubsubsocketpair=pubsubsocketpair, param=vEparameters, ownid=vEid, entitieslogger=logger)
        self.vEid = vEid

        # The load profile parameters

        model = vEparameters[VE_MODEL][VE_PARAM_LP_MODEL]

        self.model_state_default = model[VE_PARAM_LP_STATES][VE_PARAM_LP_STATES_DEFAULT]
        self.model_state_list = model[VE_PARAM_LP_STATES][VE_PARAM_LP_STATES_LIST]  # list of possible state (ON/OFF/...)
        self.model_modes = model[VE_PARAM_LP_MODES]  # list of possible mode within a state
        self.model_seq = model[VE_PARAM_LP_SEQUENCES]  # list of modes governing the activity qnd their transition

        #  Variables
        self.state = {}
        self.reset_state()  # init the state

        self.sensor_state = {VLOAD_SENSOR_KEY_QUANTITY: 0}  # power consumed (power sensor emulation)}

    def reset_state(self):
        """ When a state is over, this function is called to reset the vLoad values """
        self.state[VLOAD_KEY_STATE_START] = self.model_state_default  # the starting state (for transition)
        self.state[VLOAD_KEY_STATE_END] = self.model_state_default  # the state to reach (= initial if not transition)
        self.state[VLOAD_KEY_CURRENT_SEQUENCE_INDEX] = 0  # the current index in the sequence defining the state

        self.init_mode(self.model_seq[self.state[VLOAD_KEY_STATE_START]][self.state[VLOAD_KEY_STATE_END]].getLabelInSequence(self.state[VLOAD_KEY_CURRENT_SEQUENCE_INDEX]))
        self.state[VLOAD_KEY_CURRENT_PARAM] = None

    def reset_sequence(self):
        """ When a sequence of states is over, this function is called to reset the vLoad state """
        self.state[VLOAD_KEY_CURRENT_SEQUENCE_INDEX] = 0
        self.init_mode(0)

    def init_mode(self, seq_elem):
        """ When a new mode is picked, this function is called """
        seq = self.model_seq[self.state[VLOAD_KEY_STATE_START]][self.state[VLOAD_KEY_STATE_END]]
        self.state[VLOAD_KEY_CURRENT_SEQUENCE_INDEX] = seq_elem
        self.state[VLOAD_KEY_CURRENT_MODE] = seq.getModeInSequence(seq_elem)
        self.state[VLOAD_KEY_LAST_MODE_CHANGE] = self.current_time  # actually: beginning of the 'sequence element'
        self.state[VLOAD_KEY_MODE_DURATION] = seq.getDurationInSequence(seq_elem).generateValue()  # actually: duration of the 'sequence element'

    def p_max(self):
        """ Return the maximum power among the possible modes """
        max_val = 0
        for m in self.model_modes.keys():
            b = self.model_modes[m].bounds
            max_val = max(max_val, b[1])

        return max_val

    def _process_incoming_event(self, sender, msg):
        """ A virtual load expects incoming command, emulating its actuator """

        if not(vEntity.isCommand(msg)):
            return

        # Expecting type of command: change MODE
        if msg['PAYLOAD']['Type'] == TYPE_ACTION_MODE:
            if self.state[VLOAD_KEY_STATE_START] == self.state[VLOAD_KEY_STATE_END]:  # in activity, ready for change
                new_state = msg['PAYLOAD']['Setting'][VLOAD_MSG_STATE_KEY]
                self.state[VLOAD_KEY_STATE_END] = new_state  # incoming msg defines the targeted state

                # check whether a transition exists from the starting state:
                if self.state[VLOAD_KEY_STATE_START] is not new_state and new_state not in self.model_seq[self.state[VLOAD_KEY_STATE_START]]:
                    self.state[VLOAD_KEY_STATE_START] = self.state[VLOAD_KEY_STATE_END]  # direct transition

                # in both case, restart the sequence of VLOAD_KEY_STATE_START
                self.reset_sequence()
            else:  # the current load was in transition ... doesn't interrupt (TO BE DEFINED ...) !
                pass

            # There is an additional parameter ? If so, it's a percentage setting
            if VLOAD_MSG_QUANTITY_KEY in msg['PAYLOAD']['Setting'] and msg['PAYLOAD']['Setting'][VLOAD_MSG_QUANTITY_KEY] is not None:
                self.state[VLOAD_KEY_CURRENT_PARAM] = float(msg['PAYLOAD']['Setting'][VLOAD_MSG_QUANTITY_KEY])
                if self.state[VLOAD_KEY_CURRENT_PARAM] > 1:  # if it's a percentage
                    self.state[VLOAD_KEY_CURRENT_PARAM] /= 100.0

            return # msg has been processed: return NULL

        self.inter_function_variable['RECEIVED_MSG'] = msg

    def _update_model_state(self):
        """ Go through the current sequence and modify the consumed Power accordingly """

        # store the old quantities
        old_quantity = {}
        for s in self.sensor_state:
            old_quantity[s] = self.sensor_state[s]

        ############### CHANGE THE STATE AND THE MODE OF THE VLOAD ##################################
        seq = self.model_seq[self.state[VLOAD_KEY_STATE_START]][self.state[VLOAD_KEY_STATE_END]]

        ###### Change mode if necessary ######
        if max(self.current_time - self.state[VLOAD_KEY_LAST_MODE_CHANGE], 0) > self.state[VLOAD_KEY_MODE_DURATION]:
            self.state[VLOAD_KEY_CURRENT_SEQUENCE_INDEX] = seq.select_next_mode(self.state[VLOAD_KEY_CURRENT_SEQUENCE_INDEX])

            # a new sequence element will define a new mode
            if self.state[VLOAD_KEY_CURRENT_SEQUENCE_INDEX] is not None:
                self.init_mode(self.state[VLOAD_KEY_CURRENT_SEQUENCE_INDEX])
            else:  # End of the State (so predefined sequence)
                if self.state[VLOAD_KEY_STATE_START] is not self.state[VLOAD_KEY_STATE_END]:  # that was a transition: shift left the start and target states
                    self.state[VLOAD_KEY_STATE_START] = self.state[VLOAD_KEY_STATE_END]
                    self.reset_sequence()
                else:  # otherwise go to the default state
                    self.reset_state()

        ############### Then produce P from the mode ###############
        if self.state[VLOAD_KEY_CURRENT_MODE] in self.model_modes:
            if self.state[VLOAD_KEY_CURRENT_PARAM] is None:
                self.sensor_state[VLOAD_SENSOR_KEY_QUANTITY] = self.model_modes[self.state[VLOAD_KEY_CURRENT_MODE]].statDist.generateValue()
            else:
                bounds = self.model_modes[self.state[VLOAD_KEY_CURRENT_MODE]].bounds
                self.sensor_state[VLOAD_SENSOR_KEY_QUANTITY] = bounds[0] + float(bounds[1]-bounds[0]) * self.state[VLOAD_KEY_CURRENT_PARAM]

        #####  Now that the state and mode have been updated, the internal model update can be applied #####
        self.update_internal_state()

        for s in old_quantity:
            if self.sensor_state[s] is not None and old_quantity[s] is not None:
                self.change_accumulator[s] += (self.sensor_state[s] - old_quantity[s])

    def update_internal_state(self):
        """ For children ... not elegant but the structure is such that a function must be explicitly called
        :param in_msg: the message after vLoad processing
        :return: /
        """
        pass

    def _create_send_event(self):
        """ For any sensor linked to this vLoad, generate an event if needed """

        for sensor in self.sensor_state:
            if abs(self.change_accumulator[sensor]) >= self.change_threshold[sensor]:
                quant = {sensor: self.sensor_state[sensor]}

                self.send_event(receiver=VMID_MANAGER_ADDR, msg=json.dumps({'MSG_TYPE': EVENT_MSG_TYPE,
                                                                            'PAYLOAD': {'Type': PUB_MSG_TYPE[1],
                                                                                        'Quantity': quant}}))
                self.change_accumulator[sensor] = 0.0
