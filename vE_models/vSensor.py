__author__ = 'Olivier Van Cutsem'

import json
import csv

from conf_general import PUB_MSG_TYPE
from vEntity import vEntity
from conf_virtualization import *

# DEFINE
VE_VSENSOR_RAW_DATA = "raw_data_of_interest"
VE_VSENSOR_COEFF_RAW_DATA_TO_OUTPUT = "coeff_from_raw_to_output"

VE_VSENSOR_RAW_DATA_SOURCE_FILE = "raw_data_file"
VE_VSENSOR_RAW_DATA_SOURCE_FILE_NAME = "data_filename"
VE_VSENSOR_RAW_DATA_SOURCE_FILE_INDEX_TIME = "index_time"
VE_VSENSOR_RAW_DATA_SOURCE_FILE_INDEX_VALUE = "index_value"

VE_VSENSOR_RAW_DATA_SOURCE_FILE_LIST_VALUES = "list_values"
VE_VSENSOR_RAW_DATA_SOURCE_FILE_CURRENT_INDEX = "current_index"

VE_VSENSOR_RAW_DATA_SOURCE_VE = "raw_data_from_ve"
VE_VSENSOR_RAW_DATA_SOURCE_VE_TYPE_KEY = 0
VE_VSENSOR_RAW_DATA_SOURCE_VE_ID_KEY = 1


class vSensor(vEntity):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """

        #  Entity identification
        super(vSensor, self).__init__(pubsubsocketpair=pubsubsocketpair, param=vEparameters, ownid=vEid, entitieslogger=logger)
        self.vEid = vEid
        model = vEparameters[VE_MODEL]

        #  Raw data of interest
        #  and @in events source for raw data update, as keys()
        self.raw_data = {}
        for r_d in model[VE_VSENSOR_RAW_DATA]:
            self.raw_data[r_d] = 0

        # At least one raw data depends on a file?
        self.data_from_file = {}
        if VE_VSENSOR_RAW_DATA_SOURCE_FILE in model:
            for e in model[VE_VSENSOR_RAW_DATA_SOURCE_FILE].keys():
                self.data_from_file[e] = {}
                self.data_from_file[e][VE_VSENSOR_RAW_DATA_SOURCE_FILE_CURRENT_INDEX] = 0
                self.data_from_file[e][VE_VSENSOR_RAW_DATA_SOURCE_FILE_LIST_VALUES] = self.read_data_from_file(model[VE_VSENSOR_RAW_DATA_SOURCE_FILE][e])
                self.adapt_current_index_from_file(e)

        # When a sensor requires at least 2 raw data of the same type, distinction is done through the SENDER id
        self.data_from_ve = {}
        if VE_VSENSOR_RAW_DATA_SOURCE_VE in model:
            for e in model[VE_VSENSOR_RAW_DATA_SOURCE_VE]:
                self.data_from_ve[e] = model[VE_VSENSOR_RAW_DATA_SOURCE_VE][e]

        #  Output data
        self.state = {}
        for v in model[VE_OUTPUT_VARIABLES]:
            self.state[v] = 0

        # If specified, linear link from raw data to output values:
        self.link_from_raw_to_out = {}
        if VE_VSENSOR_COEFF_RAW_DATA_TO_OUTPUT in model:
            link = model[VE_VSENSOR_COEFF_RAW_DATA_TO_OUTPUT]
            for o_v in link.keys():
                self.link_from_raw_to_out[o_v] = {}
                for r_d in link[o_v].keys():
                    self.link_from_raw_to_out[o_v][r_d] = link[o_v][r_d]

        # for "in-the-loop" purpose
        self.inter_function_variable['NEW_VALUE'] = False

    def read_data_from_file(self, f_param):
        """ return a vector of (t,v) read from a csv file, whose column of interest are specified in f_param """
        ret = []
        try:
            # index time can be optional (first column by default)
            index_t = 0
            if VE_VSENSOR_RAW_DATA_SOURCE_FILE_INDEX_TIME in f_param:
                index_t = int(f_param[VE_VSENSOR_RAW_DATA_SOURCE_FILE_INDEX_TIME])

            # index value can be optional too (second column by default)
            index_v = 1
            if VE_VSENSOR_RAW_DATA_SOURCE_FILE_INDEX_VALUE in f_param:
                index_v = int(f_param[VE_VSENSOR_RAW_DATA_SOURCE_FILE_INDEX_VALUE])

            # read the data from the file
            ref_time = None
            with open(f_param[VE_VSENSOR_RAW_DATA_SOURCE_FILE_NAME], 'rb') as csvfile:
                csvreader = csv.reader(csvfile, delimiter=',', quotechar='|')
                for row in csvreader:
                    try:
                        t = float(row[index_t])
                        v = float(row[index_v])

                        if ref_time is None:
                            ref_time = t

                        ret.append((t-ref_time, v))
                    except:
                        pass

        except OSError:
            self.logger.error('vSensor cannot open file {0}'.format(f_param[VE_VSENSOR_RAW_DATA_SOURCE_FILE_NAME]))

        return ret

    def adapt_current_index_from_file(self, data_id):
        """ For the data vector of interest 'data_id', adapt the current index according to current time """

        if data_id not in self.data_from_file:
            return

        data_vector = self.data_from_file[data_id][VE_VSENSOR_RAW_DATA_SOURCE_FILE_LIST_VALUES]
        index = self.data_from_file[data_id][VE_VSENSOR_RAW_DATA_SOURCE_FILE_CURRENT_INDEX]
        (t, v) = data_vector[index]
        v_old = v

        # periodicity
        (t_end, v_end) = data_vector[-1]
        t_now = self.get_relative_time(t_end)

        # @ reset
        if t_now < t: index = 0

        # @ update
        (t_n, v) = data_vector[index]
        (t_n1, v) = data_vector[index+1]

        while not(t_n <= t_now <= t_n1) and index < len(data_vector)-1:
            index += 1
            (t_n, v) = data_vector[index]
            (t_n1, v) = data_vector[index+1]

        self.data_from_file[data_id][VE_VSENSOR_RAW_DATA_SOURCE_FILE_CURRENT_INDEX] = index

        return v_old is not v

    def _process_incoming_event(self, sender, msg):
        """ An event from another sensor is received: process it and return True if a new value is stored """

        self.inter_function_variable['NEW_VALUE'] = False

        if ('Type' in msg and 'Quantity' in msg) and msg['Type'] == PUB_MSG_TYPE[1]:
            quant = msg['Quantity']  # expected to be like {"LABEL": value} where LABEL can be LUM, P, HUM, etc
            for k in quant.keys():

                # Direct linking
                if k in self.raw_data:  # is this key a quantity of interest?
                    self.raw_data[k] = quant[k]
                    self.inter_function_variable['NEW_VALUE'] = True

                # The type itself is not enough: the id of the sender has to be used to identify the raw data sent
                for raw_data_key in self.data_from_ve:
                    if self.data_from_ve[raw_data_key][VE_VSENSOR_RAW_DATA_SOURCE_VE_ID_KEY] == sender and self.data_from_ve[raw_data_key][VE_VSENSOR_RAW_DATA_SOURCE_VE_TYPE_KEY] == k:
                        self.raw_data[raw_data_key] = quant[k]
                        self.inter_function_variable['NEW_VALUE'] = True

    def _update_model_state(self):
        """UPDATE raw data and LINK the output values to the raw data"""
        any_change = self.inter_function_variable['NEW_VALUE']

        # Check if this sensor needs to read its values from a static data vector
        for data_id in self.data_from_file:
            any_change = (self.adapt_current_index_from_file(data_id) or any_change)
            i = self.data_from_file[data_id][VE_VSENSOR_RAW_DATA_SOURCE_FILE_CURRENT_INDEX]
            (t, v) = self.data_from_file[data_id][VE_VSENSOR_RAW_DATA_SOURCE_FILE_LIST_VALUES][i]
            self.raw_data[data_id] = v

        # If specified, compute directly output values as a weighted sum of raw data
        if any_change:
            for o_v in self.link_from_raw_to_out:
                weight_sum = 0
                for r_d in self.link_from_raw_to_out[o_v]:
                    weight_sum += self.link_from_raw_to_out[o_v][r_d] * self.raw_data[r_d]

                self.change_accumulator[o_v] += self.state[o_v] - weight_sum
                self.state[o_v] = weight_sum

    def _create_send_event(self):
        """ Send quantity in 'state', if needed """

        for v in self.change_threshold:
            if abs(self.change_accumulator[v]) >= self.change_threshold[v]:
                quant = {v: self.state[v]}

                self.send_event(receiver=VMID_MANAGER_ADDR, msg=json.dumps({'MSG_TYPE': EVENT_MSG_TYPE,
                                                                            'PAYLOAD': {'Type': PUB_MSG_TYPE[1],
                                                                                        'Quantity': quant}}))
                self.change_accumulator[v] = 0.0
