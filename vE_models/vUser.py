__author__ = 'Olivier Van Cutsem'
import json
import numpy

from vEntity import vEntity
from conf_virtualization import *

# BMS CONNECTION
VE_USER_VE_ACTIVITIES = 've_activities'

#### DEFINE
VUSER_VE_STATE_CURRENT_TIME_ACT = 'CURRENT_TYME_IN_ACTIVITY_SEQ'
VUSER_VE_STATE_IN_ACTIVITY = 'IN_ACTIVITY'

VUSER_VE_STATE_CURRENT_ACT = 'CURRENT_ACTIVITY'
VUSER_VE_STATE_LABEL_KEY = 'STATE_LABEL'
VUSER_VE_STATE_START_TIME_KEY = 'START_TIME_ACTIVITY'
VUSER_VE_STATE_DURATION_KEY = 'DURATION_ACTIVITY'
VUSER_VE_STATE_QUANTITY = 'STATE_QUANTITY'
VUSER_VE_STATE_PARAM = 'PARAM'

VUSER_VE_STATE_LABEL_ON_KEY = "START"
VUSER_VE_STATE_LABEL_OFF_KEY = "STOP"


class vUser(vEntity):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """
        super(vUser, self).__init__(pubsubsocketpair=pubsubsocketpair, param=vEparameters, ownid=vEid, entitieslogger=logger)
        self.vEid = vEid
        model = vEparameters[VE_MODEL]

        # Constants
        # List of vLoads that will be influenced by the vUser
        self.list_of_activities = model[VE_USER_VE_ACTIVITIES]

        #  DIRECT LINK btw vEs: list of subscriber
        self.list_of_subscribed_ve = list(vEparameters[VE_SIMU_PARAM][VE_VE_SUBSCRIBED])

        #  Variables
        self.state = {}
        for vE_id in self.list_of_subscribed_ve:
            self.state[vE_id] = {}
            # INIT: each vE is OFF
            self.reset_entity(vE_id)

        #  Message frequency control variables
        for ve_id in self.list_of_subscribed_ve:
            self.change_accumulator[ve_id] = 0
            self.change_threshold[ve_id] = 0

    def reset_entity(self, vE_id):
        """
        Reset the activity state of the entity vE_id
        :param vE_id: the id of the entity to reset
        :return:
        """
        # Reset with the first activity in the sequence
        self.state[vE_id][VUSER_VE_STATE_IN_ACTIVITY] = True

        self.state[vE_id][VUSER_VE_STATE_CURRENT_ACT] = 0
        self.state[vE_id][VUSER_VE_STATE_CURRENT_TIME_ACT] = self.get_relative_time(self.list_of_activities[vE_id].period)
        self.state[vE_id][VUSER_VE_STATE_START_TIME_KEY] = self.list_of_activities[vE_id].seq_activities[self.state[vE_id][VUSER_VE_STATE_CURRENT_ACT]].start_time.generateValue()
        self.state[vE_id][VUSER_VE_STATE_DURATION_KEY] = self.list_of_activities[vE_id].seq_activities[self.state[vE_id][VUSER_VE_STATE_CURRENT_ACT]].duration.generateValue()
        self.state[vE_id][VUSER_VE_STATE_LABEL_KEY] = self.list_of_activities[vE_id].seq_activities[self.state[vE_id][VUSER_VE_STATE_CURRENT_ACT]].states[VUSER_VE_STATE_LABEL_OFF_KEY]

        self.state[vE_id][VUSER_VE_STATE_QUANTITY] = None
        self.state[vE_id][VUSER_VE_STATE_PARAM] = None

    def _process_incoming_event(self, sender, msg):
        """ A virtual user doesn't expect any incoming message """
        pass

    def _update_model_state(self):
        """ Update the 'state' vector of the virtual user, for each vE in charge of"""

        # Go over all the vE this vUser can act on
        for vE_id in self.list_of_subscribed_ve:

            activity_states = self.list_of_activities[vE_id].seq_activities[self.state[vE_id][VUSER_VE_STATE_CURRENT_ACT]].states

            # Reset the time in the activity sequence?
            last_instant = self.state[vE_id][VUSER_VE_STATE_CURRENT_TIME_ACT]
            self.state[vE_id][VUSER_VE_STATE_CURRENT_TIME_ACT] = self.get_relative_time(self.list_of_activities[vE_id].period)

            if self.state[vE_id][VUSER_VE_STATE_CURRENT_TIME_ACT] < last_instant:  # activity sequence is finished: loop
                self.reset_entity(vE_id)

            # SCHEDULE A NEW ACTIVITY?
            ve_state = self.state[vE_id]  # Actual state (user-wise) of the vE
            old_ve_state_id = ve_state[VUSER_VE_STATE_LABEL_KEY]

            # Time to schedule next mode - set off by default
            if self.state[vE_id][VUSER_VE_STATE_IN_ACTIVITY] and max(self.state[vE_id][VUSER_VE_STATE_CURRENT_TIME_ACT] - ve_state[VUSER_VE_STATE_START_TIME_KEY], 0) > ve_state[VUSER_VE_STATE_DURATION_KEY]:
                if ve_state[VUSER_VE_STATE_DURATION_KEY] > 0: # reset of normal load
                    self.state[vE_id][VUSER_VE_STATE_LABEL_KEY] = activity_states[VUSER_VE_STATE_LABEL_OFF_KEY]
                else: # pulse creation for shiftable load
                    self.state[vE_id][VUSER_VE_STATE_LABEL_KEY] = activity_states[VUSER_VE_STATE_LABEL_ON_KEY]

                self.schedule_next_activity(vE_id)

            # ON/OFF STATE ACTIVITY
            if max(self.state[vE_id][VUSER_VE_STATE_CURRENT_TIME_ACT] - self.state[vE_id][VUSER_VE_STATE_START_TIME_KEY], 0) > 0 and self.state[vE_id][VUSER_VE_STATE_IN_ACTIVITY]:
                if self.state[vE_id][VUSER_VE_STATE_DURATION_KEY] > 0 and self.state[vE_id][VUSER_VE_STATE_LABEL_KEY] == activity_states[VUSER_VE_STATE_LABEL_OFF_KEY]:  # non-Shiftable load
                    self.state[vE_id][VUSER_VE_STATE_LABEL_KEY] = activity_states[VUSER_VE_STATE_LABEL_ON_KEY]# Ignite the activity

            if old_ve_state_id != self.state[vE_id][VUSER_VE_STATE_LABEL_KEY]:
                self.change_accumulator[vE_id] = 1

                # Pulse for null duration
                if self.state[vE_id][VUSER_VE_STATE_DURATION_KEY] == 0:
                    self.state[vE_id][VUSER_VE_STATE_LABEL_KEY] = activity_states[VUSER_VE_STATE_LABEL_OFF_KEY]

            # Give the opportunity to the others to work
            self.quantized_sleep()

    def schedule_next_activity(self, vE_id):
        """
        Seek for the next activity to schedule and store the information in the state vector of the vE of concern
        :param vE_id: the id of the vE of interest
        """

        # The vUser wants to schedule the next activity BUT there is no more.. Have to want till the next period
        if self.state[vE_id][VUSER_VE_STATE_CURRENT_ACT] == self.list_of_activities[vE_id].size-1:
            self.state[vE_id][VUSER_VE_STATE_IN_ACTIVITY] = False
            return

        # Find the next activity to schedule
        while max(self.state[vE_id][VUSER_VE_STATE_CURRENT_TIME_ACT] - self.state[vE_id][VUSER_VE_STATE_START_TIME_KEY], 0) > self.state[vE_id][VUSER_VE_STATE_DURATION_KEY] and self.state[vE_id][VUSER_VE_STATE_CURRENT_ACT] < self.list_of_activities[vE_id].size-1:
            self.state[vE_id][VUSER_VE_STATE_CURRENT_ACT] += 1
            if self.list_of_activities[vE_id].seq_activities[self.state[vE_id][VUSER_VE_STATE_CURRENT_ACT]].proba < numpy.random.random_sample():
                continue

            self.state[vE_id][VUSER_VE_STATE_START_TIME_KEY] = self.list_of_activities[vE_id].seq_activities[self.state[vE_id][VUSER_VE_STATE_CURRENT_ACT]].start_time.generateValue()
            self.state[vE_id][VUSER_VE_STATE_DURATION_KEY] = self.list_of_activities[vE_id].seq_activities[self.state[vE_id][VUSER_VE_STATE_CURRENT_ACT]].duration.generateValue()

    def _create_send_event(self):
        """ For each vE in charge of, check if it's the right time to send a command """

        for vE_id in self.list_of_subscribed_ve:
            if self.change_accumulator[vE_id] > self.change_threshold[vE_id]:

                type_action = None

                #TODO: distribution for the parameter? complex depending on the kind of param ...
                state_to_set = self.state[vE_id][VUSER_VE_STATE_LABEL_KEY]
                param = self.list_of_activities[vE_id].seq_activities[self.state[vE_id][VUSER_VE_STATE_CURRENT_ACT]].param

                # If the duration is > 0, it means the activity if fully controlled by the vUser
                # The default type of command is to change the mode, though it might be different if specified
                if self.state[vE_id][VUSER_VE_STATE_DURATION_KEY] > 0:
                    type_action = TYPE_ACTION_MODE
                    if param is None:
                        self.state[vE_id][VUSER_VE_STATE_QUANTITY] = None
                    else:
                        if type(param) is dict and "type_command" in param:  # the type of action is specified
                            type_action = param["type_command"]
                            if "value" in param:
                                if state_to_set == self.list_of_activities[vE_id].seq_activities[self.state[vE_id][VUSER_VE_STATE_CURRENT_ACT]].states[VUSER_VE_STATE_LABEL_ON_KEY]:
                                    self.state[vE_id][VUSER_VE_STATE_QUANTITY] = param["value"]
                                else:
                                    self.state[vE_id][VUSER_VE_STATE_QUANTITY] = 0  # OFF and value = 0
                        else:
                            self.state[vE_id][VUSER_VE_STATE_QUANTITY] = param
                    self.state[vE_id][VUSER_VE_STATE_PARAM] = None
                else:  # deferrable load !
                    type_action = TYPE_ACTION_START
                    state_to_set = self.list_of_activities[vE_id].seq_activities[self.state[vE_id][VUSER_VE_STATE_CURRENT_ACT]].states[VUSER_VE_STATE_LABEL_ON_KEY]
                    if type(param) is not dict:  # only t_end
                        self.state[vE_id][VUSER_VE_STATE_QUANTITY] = param # param is a distribution of t_end_max
                    else:
                        self.state[vE_id][VUSER_VE_STATE_QUANTITY] = param["max_delay_end"]
                        self.state[vE_id][VUSER_VE_STATE_PARAM] = param["dim_value"]

                set = {VLOAD_MSG_STATE_KEY: state_to_set,
                       VLOAD_MSG_QUANTITY_KEY: self.state[vE_id][VUSER_VE_STATE_QUANTITY],
                       VLOAD_MSG_PARAM_KEY: self.state[vE_id][VUSER_VE_STATE_PARAM]}
                payload = self.generate_command(type_action, set)

                self.send_event(receiver=vE_id, msg=json.dumps({'MSG_TYPE': COMMAND_MSG_TYPE, 'PAYLOAD': payload}))
                self.change_accumulator[vE_id] = 0

    def _update_sleep_interval(self):
        """ Modify dt to be the interval until the next event (i.e. begin or end of an activity) """
        min_dt = None

        # Adapt the sleeping time to be the minimum required one, such that it's useless to wake up in between
        for vE_id in self.list_of_subscribed_ve:

            # next start?
            if self.state[vE_id][VUSER_VE_STATE_CURRENT_TIME_ACT] < self.state[vE_id][VUSER_VE_STATE_START_TIME_KEY]:
                dt = self.state[vE_id][VUSER_VE_STATE_START_TIME_KEY]-self.state[vE_id][VUSER_VE_STATE_CURRENT_TIME_ACT]
            else:  # next end
                if self.state[vE_id][VUSER_VE_STATE_IN_ACTIVITY]:
                    dt = (self.state[vE_id][VUSER_VE_STATE_START_TIME_KEY] + self.state[vE_id][VUSER_VE_STATE_DURATION_KEY]) - self.state[vE_id][VUSER_VE_STATE_CURRENT_TIME_ACT]
                else:
                    dt = self.list_of_activities[vE_id].period - self.state[vE_id][VUSER_VE_STATE_CURRENT_TIME_ACT]
            if min_dt is None or dt < min_dt:
                min_dt = dt

        # if there is a value to set, otherwise call the generic function
        # if min_dt is not None:
        #     self.dt = min_dt
        # else:
        #     super(vUser, self)._update_sleep_interval()
        super(vUser, self)._update_sleep_interval()
