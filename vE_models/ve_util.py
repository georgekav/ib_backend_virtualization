__author__ = 'Olivier Van Cutsem'

import matplotlib.pyplot as plt
import numpy

class PiecewiseFunction:

    def __init__(self, xy):
        self.lut = xy

    def get_fct_value(self, x):
        for _int in self.lut:
            (x1, y1) = _int[0]
            (x2, y2) = _int[1]

            # In the right bound?
            if x1 <= x <= x2:
                # Linear interpolation
                a = float(y2-y1)/float(x2-x1)
                b = y2 - a * x2
                return a * x + b

    def plot(self):
        x=[]
        y=[]

        for _int in self.lut:
            (x1, y1) = _int[0]
            (x2, y2) = _int[1]
            x.append(x1)
            x.append(x2)
            y.append(y1)
            y.append(y2)

        plt.plot(x, y)
        plt.show()


class statDistribution:
    modelsList = ['NORM', 'UNIF']

    def __init__(self, t, param):

        self.statModel = t
        if t not in self.modelsList:
            self.statModel = self.modelsList[0]

        self._param = param

    @property
    def model(self):
        return self.statModel

    @property
    def param(self):
        return self._param

    def generateValue(self):
        if self.statModel == self.modelsList[0]:
            return self._param[0] + self._param[1] * numpy.random.randn()
        elif self.statModel == self.modelsList[1]:
            return self._param[0] + (self._param[1] - self._param[0]) * numpy.random.random_sample()

    @property
    def equivalent_quantity(self):
        if self.statModel == self.modelsList[0]:
            return self._param[0]
        elif self.statModel == self.modelsList[1]:
            return (self._param[0] + self._param[1])/2

    def __le__(self, stat_distr):
        return self.equivalent_quantity < stat_distr.equivalent_quantity


