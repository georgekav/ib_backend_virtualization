__author__ = 'Georgios Lilis'
from vEntity import vEntity
import json, logging
from conf_virtualization import *
from conf_general import PUB_MSG_TYPE

class vBenchmark(vEntity):
    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """
        super(vBenchmark, self).__init__(pubsubsocketpair=pubsubsocketpair, param=vEparameters, ownid=vEid, entitieslogger=logger)
        self.vEid = vEid

        self.receiving_timer = 0
        self.computing_timer = 0
        self.sending_timer = 0
        self.sleeping_timer = 0
        self.timer_counter = 1

    def _process_incoming_event(self, sender, payload):
        #Nothing for the vBenchmark to do with it
        print 'vEntity: {0} MSG: {1} from {2}'.format(self.vEid, payload, sender)
        pass

    def _update_model_state(self):
        """Abstract method of _update_model_state that need to be overloaded by the children class.
        USE vEntity.quantized_sleep() if computing and updating the model state takes too long.
        :param pr_msg: the processed message
        :return the new model variables"""
        pass

    def _create_send_event(self):
        self.log(logging.DEBUG, 'Sending event: {0}'.format(self.health[HEALTH_ITERATIONS_KEY]))
        self.send_event(receiver=VMID_MANAGER_ADDR, msg=json.dumps({'MSG_TYPE' : EVENT_MSG_TYPE, 'PAYLOAD' : {'Type': PUB_MSG_TYPE[1], 'Quantity': self.health[HEALTH_ITERATIONS_KEY]}}))
        pass

    def _shutdown_seq(self):
        #MANDATORY, send the state as an object in mysate. After the return from this place the vE terminates.
        self.mystate = {'Receiving' : self.receiving_timer, 'Computing':self.computing_timer,
                        'Sending': self.sending_timer, 'Sleeping': self.sleeping_timer}
        self.save_state(state=self.mystate)
        self.log(logging.DEBUG, 'Total Average taken for: Receiving {0}s || Computing {1}s || Sending {2}s || Sleeping {3}s'
                 .format(round(self.receiving_timer/self.timer_counter,3), round(self.computing_timer/self.timer_counter, 3),
                         round(self.sending_timer/self.timer_counter, 3), round(self.sleeping_timer/self.timer_counter, 3)))
        ##########################

    def _startup_seq(self, savedstate):
        self.mystate = 'mystate {0:06d}'.format(self.vEid)
