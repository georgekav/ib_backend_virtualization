__author__ = 'Olivier Van Cutsem'

import math
import numpy as np
from vStorage import vStorage

# KEYS FOR BMS
VE_BAT_MODEL_PARAM_CAPA_RATIO = 'capacity_ratio_b'
VE_BAT_MODEL_PARAM_RATE_K = 'rate_k'

# Constants
VE_BAT_MODEL_NB_ITER_INIT = 100
VE_BAT_MODEL_EPS_E = 0.001

class vStorage_kibam(vStorage):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """

        #  Entity identification
        super(vStorage_kibam, self).__init__(pubsubsocketpair=pubsubsocketpair, vEparameters=vEparameters, vEid=vEid, logger=logger)

        model = self.model_param

        # Internal energy state
        self.e1 = 0
        self.e2 = 0

        # Peak power charge and discharge:
        self.b = model[VE_BAT_MODEL_PARAM_CAPA_RATIO]
        self.k = model[VE_BAT_MODEL_PARAM_RATE_K]

        self.init_internal_energy()

    def init_internal_energy(self):

        # Reach the initial SoC by charging as much as possible
        soc_init = self.state['SOC']
        self.state['SOC'] = 0

        self.model_change['NEW_DELTA_T'] = self.change_threshold['SOC'] * self.total_capa / self.max_power_ch

        while self.state['SOC'] < soc_init:
            self.state['P'] = self.max_power_ch
            self.update_soc(self.model_change['NEW_DELTA_T'])

        # Stabilize internal energy with P = 0
        e1_prev = self.e1
        e2_prev = self.e2
        self.state['P'] = 0
        i = 0
        while i < VE_BAT_MODEL_NB_ITER_INIT:
            self.update_soc(self.model_change['NEW_DELTA_T'])

            # When the internal energy is stabilized
            if abs(e1_prev - self.e1) < VE_BAT_MODEL_EPS_E * e1_prev or \
               abs(e2_prev - self.e2) < VE_BAT_MODEL_EPS_E * e2_prev:
                break

            e1_prev = self.e1
            e2_prev = self.e2
            i += 1

    def update_soc(self, dt):
        """
        KiBam model, with discretization time step dt
        :return:
        """

        ### P is negative -> charging state
        power_ch_disch = -self.state['P']

        k_exp = math.exp(-self.k * dt)
        e = self.e1 + self.e2
        self.e1 = self.e1 * k_exp + e * self.b * (1-k_exp) - power_ch_disch * ((1-k_exp) + self.b * (self.k*dt - 1 + k_exp))/self.k
        self.e2 = self.e2 * k_exp + e * (1 - self.b) * (1 - k_exp) - power_ch_disch * (1 - self.b) * (self.k*dt - 1 + k_exp)/self.k

        # Update SoC
        self.state['SOC'] = (self.e1 + self.e2) / self.total_capa

    def get_max_power(self, p):
        dt = self.model_change['NEW_DELTA_T']
        k_exp = math.exp(-self.k * dt)
        e = self.e1 + self.e2
        den = 1 - k_exp + self.b * (self.k * dt - 1 + k_exp)

        if p > 0:  # the battery has to absorb power
            return -(-self.k*self.b * self.total_capa + self.k * self.e1 * k_exp + e * self.k*self.b * (1-k_exp)) / den
        elif p < 0:  # the battery has to give power
            return -(self.k * self.e1 * k_exp + e * self.k*self.b * (1-k_exp)) / den
