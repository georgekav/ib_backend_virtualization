__author__ = 'Olivier Van Cutsem'

import math
import numpy as np
from vStorage import vStorage

# KEYS FOR BMS
VE_BAT_MODEL_PARAM_EFF_SOC = 'soc_efficiency'
VE_BAT_MODEL_PARAM_RESIST = 'internal_resistance'
VE_BAT_MODEL_PARAM_VOLT_SOC = 'norm_voltage_soc'
VE_BAT_MODEL_PARAM_MAX_P_CH = 'peak_power_charge'
VE_BAT_MODEL_PARAM_MAX_P_DISCH = 'peak_power_discharge'
VE_BAT_MODEL_PARAM_VOLT = 'nom_voltage'


class vStorage_integrativeSoC(vStorage):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """

        #  Entity identification
        super(vStorage_integrativeSoC, self).__init__(pubsubsocketpair=pubsubsocketpair, vEparameters=vEparameters, vEid=vEid, logger=logger)

        model = self.model_param

        # Peak power charge qnd discharge:
        self.max_power_ch = max(self.max_power_ch)
        self.max_power_disch = -max(self.max_power_disch)

        # Efficiency vs stage of charge (SOC):
        self.av_eff_soc = np.dot(self.disch_efficiency, self.ch_efficiency)

        # Internal resistance
        self.r_in = model[VE_BAT_MODEL_PARAM_RESIST]

        # output nom volt and voltage VS SOC for a 3.7-V bank
        self.v_nom = model[VE_BAT_MODEL_PARAM_VOLT]
        self.volt_vs_SOC = model[VE_BAT_MODEL_PARAM_VOLT_SOC]

        #  Adapt the output voltage for this battery, according to v_nom
        i = 0
        for v in self.volt_vs_SOC:
            self.volt_vs_SOC[i] = (v / 3.7) * self.v_nom
            i += 1

    def update_soc(self, dt):
        """
        Here is a simple integrative model. Normally this function should be overloaded
        :return:
        """
        index_soc = int(math.floor(self.state['SOC'] * 10))
        if index_soc + 1 >= len(self.volt_vs_SOC):
            index_soc = len(self.volt_vs_SOC)-2

        beta_soc = (self.state['SOC'] * 10 - math.floor(self.state['SOC'] * 10))
        volt_interp = (1 - beta_soc) * self.volt_vs_SOC[index_soc] + beta_soc * self.volt_vs_SOC[index_soc + 1]

        av_eff = (1 - beta_soc) * self.av_eff_soc[index_soc] + beta_soc * self.av_eff_soc[index_soc + 1]
        eff_p = math.sqrt(av_eff) * (abs(self.state['P']) - self.r_in * abs(self.state['P']) / volt_interp * abs(self.state['P']) / volt_interp) / abs(self.state['P'])

        #  UPDATE SOC
        self.state['SOC'] += eff_p * self.state['P'] * dt / (self.total_capa * 3600)

    def get_max_power(self, p):
        if p > 0:  # the battery has to absorb power
            return self.max_power_ch
        elif p < 0:  # the battery has to give power
            return self.max_power_disch
