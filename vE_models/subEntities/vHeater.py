__author__ = 'Olivier Van Cutsem'

from vLoad import *
from ve_util import PiecewiseFunction

# Global LUX luminosity when full power
VHEATER_YIELD = 0.95


class vHeater(vLoad):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """
        super(vHeater, self).__init__(pubsubsocketpair, vEparameters, vEid, logger)
        self.sensor_state['P_HEAT'] = 0


    def update_internal_state(self):
        """ From P_in, associate output variables """

        #TODO: more complex model with the temperature? DEPENDS ON THE HVAC and where is the intelligence
        self.sensor_state['P_HEAT'] = VHEATER_YIELD * self.sensor_state[VLOAD_SENSOR_KEY_QUANTITY]


