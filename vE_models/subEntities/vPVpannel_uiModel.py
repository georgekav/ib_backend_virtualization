__author__ = 'Olivier Van Cutsem'

from vGeneration import *

# KEYS FOR MODEL
VE_GEN_REF_PARAM = 'reference_param'
VE_GEN_REF_PARAM_IRR = 'ref_irradiance'
VE_GEN_REF_PARAM_TEMP = 'ref_temperature'
VE_GEN_REF_PARAM_TEMP_NTC = 'temperature_normal_cond'

VE_GEN_PV_PARAM = 'pv_param'
VE_GEN_PV_PARAM_STD_VOLTAGE = "std_cond_voltage"
VE_GEN_PV_PARAM_STD_CURRENT_TEMP_COEFF = "current_temperature_coeff"
VE_GEN_PV_PARAM_STD_CURRENT = "std_cond_current"
VE_GEN_PV_PARAM_STD_VOLTAGE_TEMP_COEFF = "voltage_temperature_coeff"
VE_GEN_PV_PARAM_NB_CELLS = 'nb_cells'

# CONSTANT
LUM_TO_RAD_CONST_FACTOR = 93


class vPVpannel_uiModel(vGeneration):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """
        super(vPVpannel_uiModel, self).__init__(pubsubsocketpair, vEparameters, vEid, logger)

        model = vEparameters[VE_MODEL]

        # TODO: check the validity of the parameters
        self.g_ref = model[VE_GEN_REF_PARAM][VE_GEN_REF_PARAM_IRR]  # W/m2
        self.t_ref = model[VE_GEN_REF_PARAM][VE_GEN_REF_PARAM_TEMP]  # degC
        self.t_ref_ntc = model[VE_GEN_REF_PARAM][VE_GEN_REF_PARAM_TEMP_NTC]

        self.nb_pv_array = model[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_NB_CELLS]
        self.I_P = model[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_CURRENT]  # Amp
        self.U_P = model[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_VOLTAGE]  # Volt
        self.di_p_dT = model[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_CURRENT_TEMP_COEFF]  # %/degC
        self.du_p_dT = model[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_VOLTAGE_TEMP_COEFF]  # %/degC

    def _update_model_state(self):
        """ From P, associate output variables """
        
        super(vPVpannel_uiModel, self)._update_model_state()

        if self.in_sensor['TEMP'] is None or self.in_sensor['LUM'] is None:
            return

        temp = self.in_sensor['TEMP']
        rad = self.in_sensor['LUM']/LUM_TO_RAD_CONST_FACTOR
        k = rad/self.g_ref

        old_p = self.state['P']

        T_cell = temp + k * (self.t_ref_ntc - 20)
        u_mppt = self.U_P * (1 + self.du_p_dT * (T_cell - self.t_ref))
        i_mppt = self.I_P * k * (1 + self.di_p_dT * (T_cell - self.t_ref))

        #  The power is given to the system, so negative ???

        self.state['P'] = -self.nb_pv_array * u_mppt * i_mppt

        self.state['P'] = min(0, self.state['P'])
        self.change_accumulator['P'] += (self.state['P'] - old_p)
