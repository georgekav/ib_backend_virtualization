__author__ = 'Olivier Van Cutsem'

from vGeneration import *

# KEYS FOR MODEL
VE_GEN_REF_PARAM = 'reference_param'
VE_GEN_REF_PARAM_IRR = 'ref_irradiance'
VE_GEN_REF_PARAM_TEMP = 'ref_temperature'

VE_GEN_PV_PARAM = 'pv_param'
VE_GEN_PV_PARAM_STD_POWER = 'std_cond_power'
VE_GEN_PV_PARAM_LOSS = 'losses'
VE_GEN_PV_PARAM_TEMP_COEFF = 'temperature_coeff'
VE_GEN_PV_PARAM_NB_CELLS = 'nb_cells'


# CONSTANT
LUM_TO_RAD_CONST_FACTOR = 93


class vPVpannel(vGeneration):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """
        super(vPVpannel, self).__init__(pubsubsocketpair, vEparameters, vEid, logger)

        model = vEparameters[VE_MODEL]

        # TODO: check the validity of the parameters
        self.g_ref = model[VE_GEN_REF_PARAM][VE_GEN_REF_PARAM_IRR]  # W/m2
        self.t_ref = model[VE_GEN_REF_PARAM][VE_GEN_REF_PARAM_TEMP]  # degC

        self.p_stc = model[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_POWER]  # Watt
        self.losses = model[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_LOSS]  # ohm
        self.c_p = model[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_TEMP_COEFF]  # %/degC
        self.nb_pv_array = model[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_NB_CELLS]

    def _update_model_state(self):
        """ From P, associate output variables """
        
        super(vPVpannel, self)._update_model_state()

        if self.in_sensor['TEM'] is None or self.in_sensor['LUM'] is None:
            return

        temp = self.in_sensor['TEM']
        rad = self.in_sensor['LUM']/LUM_TO_RAD_CONST_FACTOR
        k = rad/self.g_ref

        old_p = self.state['P']

        #  The power is given to the system, so negative
        self.state['P'] = - self.nb_pv_array * self.p_stc * (k + k * self.c_p/100 * (temp-self.t_ref) - self.losses)
        self.state['P'] = min(0, self.state['P'])
        self.change_accumulator['P'] += (self.state['P'] - old_p)
