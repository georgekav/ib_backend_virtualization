__author__ = 'Olivier Van Cutsem'

from vLoad import *
from ve_util import PiecewiseFunction

# Global LUX luminosity when full power
VLAMP_LUX_COEFF_KEY = "max_lux"
VLAMP_P_TO_LUM_NORM = [[(0, 0), (5, 0)], [(5, 5), (15, 5)], [(15, 10), (90, 100)], [(90, 100), (100, 100)]]

class vLamp(vLoad):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """
        super(vLamp, self).__init__(pubsubsocketpair, vEparameters, vEid, logger)

        self.sensor_state['LUM'] = None
        self.lux_coeff = vEparameters[VE_MODEL][VLAMP_LUX_COEFF_KEY]

        # p max
        self.up_bound = self.p_max()

        # x to y
        self.lut = PiecewiseFunction(VLAMP_P_TO_LUM_NORM)

    def update_internal_state(self):
        """ From P, associate output variables """

        # From p to lum
        p_norm = 100*self.sensor_state[VLOAD_SENSOR_KEY_QUANTITY]/self.up_bound  # work with percentage
        lum_norm = self.lut.get_fct_value(p_norm)

        self.sensor_state['LUM'] = (lum_norm * self.lux_coeff) / 100




