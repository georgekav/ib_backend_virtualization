__author__ = 'Olivier Van Cutsem'

from vLoad import *

# Global LUX luminosity when full power


class vShiftable(vLoad):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """
        super(vShiftable, self).__init__(pubsubsocketpair, vEparameters, vEid, logger)

    def update_internal_state(self, in_msg):
        #TODO: process message that comes from the vUser aiming at programming the load
        in_msg = self.inter_function_variable['RECEIVED_MSG']

        if in_msg is None or 'PAYLOAD' not in in_msg or 'Type' not in in_msg['PAYLOAD']:
            return

        # this is a signal for programming the load start
        if in_msg['PAYLOAD']['Type'] == TYPE_ACTION_START:
            desired_new_state = in_msg['PAYLOAD']['Setting'][VLOAD_MSG_STATE_KEY]
            max_delay_to_end = in_msg['PAYLOAD']['Setting'][VLOAD_MSG_QUANTITY_KEY]
            desired_param = in_msg['PAYLOAD']['Setting'][VLOAD_MSG_PARAM_KEY]

            # Approximation of the mean duration of the whole sequence
            duration = 0
            seq = self.model_seq[desired_new_state][desired_new_state]
            for i in range(0, seq.size):
                duration += seq.getDurationInSequence(i).equivalent_quantity

            max_desired_start = (self.current_time + max_delay_to_end) - duration

            quant = {'Type': PUB_MSG_TYPE[0], 'Quantity': {'Type': 'start_time',
                                                           'Value': max_desired_start,
                                                           'state': desired_new_state,
                                                           'param': desired_param}}
            self.send_event(receiver=VMID_MANAGER_ADDR, msg=json.dumps({'MSG_TYPE': EVENT_MSG_TYPE, 'PAYLOAD': quant}))
