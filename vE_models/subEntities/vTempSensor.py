__author__ = 'Olivier Van Cutsem'

from vSensor import *

# Global LUX luminosity when full power
VTEMPSENSOR_CURRENT_TEMP = 'current_temp'
VTEMPSENSOR_CURRENT_TEMP_TIMESTAMP = 'current_temp_timestamp'

# NORMALIZED VALUES and CONSTANTS
VTEMPSENSOR_NORM_TEMP = 25

AIR_MASS = 5*3*3*1000
R_EQ_HOUSE = 0.1
AIR_HEAT_CAPA = 1.005

class vTempSensor(vSensor):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """
        super(vTempSensor, self).__init__(pubsubsocketpair, vEparameters, vEid, logger)

        self.simu_state = {VTEMPSENSOR_CURRENT_TEMP: VTEMPSENSOR_NORM_TEMP, VTEMPSENSOR_CURRENT_TEMP_TIMESTAMP: self.current_time}
        self.raw_data["TEMP_OUT"] = VTEMPSENSOR_NORM_TEMP
        self.simu_state["TEM"] = VTEMPSENSOR_NORM_TEMP

    def _update_model_state(self):
        """ From various P, update the temperature """

        last_t = self.simu_state[VTEMPSENSOR_CURRENT_TEMP_TIMESTAMP]
        self.simu_state[VTEMPSENSOR_CURRENT_TEMP_TIMESTAMP] = self.current_time
        dt = max(self.simu_state[VTEMPSENSOR_CURRENT_TEMP_TIMESTAMP] - last_t, 0)

        # TODO: mathematical model MORE PRECISE
        # Power flows: maybe more complex
        p_in = self.raw_data["HEAT_BLIND"] + self.raw_data["HEAT_HVAC"]

        # Power flows: maybe more complex
        p_out = (self.simu_state[VTEMPSENSOR_CURRENT_TEMP] - self.raw_data["TEMP_OUT"]) / R_EQ_HOUSE

        # Update new temperature
        aux = self.simu_state[VTEMPSENSOR_CURRENT_TEMP]
        self.simu_state[VTEMPSENSOR_CURRENT_TEMP] += dt * (p_in - p_out) / (AIR_HEAT_CAPA * AIR_MASS)
        self.change_accumulator["TEM"] += self.simu_state[VTEMPSENSOR_CURRENT_TEMP] - aux

        # output data
        self.state["TEM"] = self.simu_state[VTEMPSENSOR_CURRENT_TEMP]

