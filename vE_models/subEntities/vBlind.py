__author__ = 'Olivier Van Cutsem'

from vActuator import *
from ve_util import PiecewiseFunction

# Global LUX luminosity when full power
VBLIND_LUX_COEFF_KEY = "coeff_lum_in"
VBLIND_HEAT_COEFF_KEY = "coeff_lum_to_heat"
VLAMP_ANGLE_TO_LUM_NORM = [[(0, 0), (5, 0)], [(5, 5), (15, 5)], [(15, 10), (90, 100)], [(90, 100), (100, 100)]]
VLAMP_ANGLE_TO_HEAT_NORM = [[(0, 0), (5, 0)], [(5, 5), (15, 5)], [(15, 10), (90, 90)], [(90, 90), (100, 95)]]

VBLIND_MIN_ANGLE = 0
VBLIND_MAX_ANGLE = 90

class vBlind(vActuator):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """
        super(vBlind, self).__init__(pubsubsocketpair, vEparameters, vEid, logger)

        # Closed position
        self.state[VE_VACTUATOR_COMMAND_TIME_RECEIVED] = self.current_time
        self.state[VE_VACTUATOR_COMMAND_PARAM] = 0

        # Sensor of interest
        self.sensor_state = dict()
        self.sensor_state['LUM'] = None
        self.sensor_state['TEM'] = None

        # Constants
        self.lux_coeff = vEparameters[VE_MODEL][VBLIND_LUX_COEFF_KEY]
        self.heat_coeff = vEparameters[VE_MODEL][VBLIND_HEAT_COEFF_KEY]

        self.lut_lum = PiecewiseFunction(VLAMP_ANGLE_TO_LUM_NORM)
        self.lut_heat = PiecewiseFunction(VLAMP_ANGLE_TO_HEAT_NORM)

    def _process_incoming_event(self, sender, msg):
        """Abstract method of _model_setup that need to be overloaded by the children class.
        bla bla bla."""

        ret = super(vBlind, self)._process_incoming_event(sender, msg)

        # Constraints on the angles
        if ret:
            self.state[VE_VACTUATOR_COMMAND_PARAM] = min(max(self.state[VE_VACTUATOR_COMMAND_PARAM], VBLIND_MIN_ANGLE), VBLIND_MAX_ANGLE)

        # LUM and TEMP received from the outside
        if not(vEntity.isCommand(msg)) and msg['Type'] == PUB_MSG_TYPE[1]:
            if 'LUM' in msg['Quantity']:
                self.sensor_state['LUM'] = max(msg['Quantity']['LUM'], 0)
            if 'TEM' in msg['Quantity']:
                self.sensor_state['TEM'] = msg['Quantity']['TEM']

        return ret

    def _update_model_state(self):
        """ From P, associate output variables """

        super(vBlind, self)._update_model_state()

        angle_norm = self.state[VE_VACTUATOR_COMMAND_PARAM]

        if self.sensor_state['LUM'] is not None and angle_norm is not None:
            # Link LUM to angle
            aux = self.output_var['LUM']
            self.output_var['LUM'] = (self.lut_lum.get_fct_value(angle_norm) * self.sensor_state['LUM'] * self.lux_coeff) / 100
            self.change_accumulator['LUM'] += (self.output_var['LUM'] - aux)

            # Link HEAT to angle
            aux = self.output_var['P_HEAT']
            self.output_var['P_HEAT'] = (self.lut_heat.get_fct_value(angle_norm) * self.sensor_state['LUM'] * self.heat_coeff) / 100
            self.change_accumulator['P_HEAT'] += (self.output_var['P_HEAT'] - aux)
