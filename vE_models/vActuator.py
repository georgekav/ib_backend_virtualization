__author__ = 'Olivier Van Cutsem'

import json

from conf_general import PUB_MSG_TYPE, ACTUATOR_API_TYPES
from vEntity import vEntity
from conf_virtualization import *

# DEFINE
VE_VACTUATOR_COMMAND_STATE = "command"
VE_VACTUATOR_COMMAND_ACK = "ack"

VE_VACTUATOR_COMMAND_TYPE = "action"
VE_VACTUATOR_COMMAND_TIME_RECEIVED = "time_rec"
VE_VACTUATOR_COMMAND_PARAM = "time_rec"
VE_VACTUATOR_OUTPUT_VARIABLE = "output_variable"
VE_VACTUATOR_IN_TRANSITION = "states_transition"


class vActuator(vEntity):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """

        #  Entity identification
        super(vActuator, self).__init__(pubsubsocketpair=pubsubsocketpair, param=vEparameters, ownid=vEid, entitieslogger=logger)
        self.vEid = vEid
        model = vEparameters[VE_MODEL]

        # Current command = last received command
        self.state = {VE_VACTUATOR_COMMAND_TIME_RECEIVED: None, VE_VACTUATOR_COMMAND_PARAM: None, VE_VACTUATOR_IN_TRANSITION: False}

        #  Output data
        self.output_var = {}
        for v in model[VE_OUTPUT_VARIABLES]:
            self.output_var[v] = 0

        # Type of command this actuator can receive
        self.type_command = ACTUATOR_API_TYPES[0]
        if VE_VACTUATOR_COMMAND_TYPE in model and model[VE_VACTUATOR_COMMAND_TYPE] in ACTUATOR_API_TYPES:
            self.type_command = model[VE_VACTUATOR_COMMAND_TYPE]

        # Acknowledge upon reception?
        self.acknowledgement = model[VE_VACTUATOR_COMMAND_ACK] == 1

        # for "in-the-loop" purpose
        self.inter_function_variable['NEW_VALUE'] = False

    def _process_incoming_event(self, sender, msg):
        """ Process the incoming commands: return TRUE if a new value is received"""

        self.inter_function_variable['NEW_VALUE'] = False
        type_rec = None

        if vEntity.isCommand(msg):
            type_rec = msg['PAYLOAD']['Type']
        else:
            return

        #TODO: here, implemented only for DIMMER
        if type_rec == self.type_command:
            self.change_accumulator[VE_VACTUATOR_COMMAND_STATE] = self.state[VE_VACTUATOR_COMMAND_TIME_RECEIVED] - self.current_time
            self.state[VE_VACTUATOR_COMMAND_TIME_RECEIVED] = self.current_time

            if 'Quantity' in msg['PAYLOAD']['Setting']:
                self.state[VE_VACTUATOR_COMMAND_PARAM] = msg['PAYLOAD']['Setting']['Quantity']
            else:
                self.state[VE_VACTUATOR_COMMAND_PARAM] = None

            self.inter_function_variable['NEW_VALUE'] = True

    def _update_model_state(self):
        """UPDATE raw data and LINK the output values to the raw data"""
        if not (self.inter_function_variable['NEW_VALUE'] or self.state[VE_VACTUATOR_IN_TRANSITION]):  # for "instantaneous" command
            return

        # Method to be overwritten
        pass

    def _create_send_event(self):
        """ Send quantity in 'output_var', if needed """

        if self.acknowledgement and abs(self.change_accumulator[VE_VACTUATOR_COMMAND_STATE]) >= self.change_threshold[VE_VACTUATOR_COMMAND_STATE]:
            # Acknowledge the received msg
            self.send_event(receiver=VMID_MANAGER_ADDR, msg=json.dumps({'MSG_TYPE': EVENT_MSG_TYPE, 'PAYLOAD': {'Type': PUB_MSG_TYPE[0], 'Setting': self.state[VE_VACTUATOR_COMMAND_PARAM]}}))
            self.change_accumulator[VE_VACTUATOR_COMMAND_STATE] = 0.0

        for v in self.output_var:
            if abs(self.change_accumulator[v]) >= self.change_threshold[v]:
                quant = {v: self.output_var[v]}
                self.send_event(receiver=VMID_MANAGER_ADDR, msg=json.dumps({'MSG_TYPE': EVENT_MSG_TYPE, 'PAYLOAD': {'Type': PUB_MSG_TYPE[1], 'Quantity': quant}}))
                self.change_accumulator[v] = 0.0
