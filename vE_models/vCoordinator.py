__author__ = 'Olivier Van Cutsem'

import json
import csv
import copy

from conf_general import PUB_MSG_TYPE
from vEntity import vEntity
from conf_virtualization import *

# BMS fields
VE_VCOORD_LIST_VENTITIES = "registered_entities"
VE_VCOORD_LIST_EXT_MODULES = "registered_modules"
VE_VCOORD_LIST_DEPENDENCY = "list_dependency"

class vCoordinator(vEntity):

    def __init__(self, pubsubsocketpair, vEparameters, vEid, logger):
        """
        :param pubsubsocketpair: The pair of the two sockets that connects the vE to the vMid. It is just passed to the constructor
        of the parent
        :param logger: The logger object shared by all the vEntities.
        :param vEid: The unique ID of the vE as defined by the openBMS. It is just passed to the constructor
        of the parent for filtering of event. It is also used by the child if it needs to address other vE.
        :param vEparameters: The configuration parameters of vE as defined by the openBMS. It is used by the constructor
        of the child to recreate the virtual model.
        """

        #  Entity identification
        super(vCoordinator, self).__init__(pubsubsocketpair=pubsubsocketpair, param=vEparameters, ownid=vEid, entitieslogger=logger)

        model = vEparameters[VE_MODEL]

        # The dependency between the entities, in order to schedule properly the vEs
        self.list_dependency = copy.deepcopy(model[VE_VCOORD_LIST_DEPENDENCY])
        self.remove_dependency_loop()  # Remove the cycles !

        # The list of vEs that participate to the simulation
        self.list_ve = model[VE_VCOORD_LIST_VENTITIES]
        if self.vEid in self.list_ve:  # remove myself from the list of entities I'm in charge of
            self.list_ve.pop(self.vEid)

        # The list of external modules that participate to the simulation
        self.list_modules = model[VE_VCOORD_LIST_EXT_MODULES]  #TODO: there should be a dependency list as well..

        # VARIABLE AND STATES
        self.v_pool_done = False  # all the closed system is done (all the ve are done)
        self.module_already_contacted_when_done = False
        self.reached_end_simu_step = False  # the simulation step is finished
        self.received_ready_ve_signal = list()
        self.received_ready_modules_signal = list()

        self.routing_msg = dict()  # buffer for incoming messages
        self.list_remaining_ve = self.reset_pending_list()  # This list holds the vE that still must be triggered

    def reset_pending_list(self):

        # The dependency table incluences the pending list
        pend_list = copy.deepcopy(self.list_dependency)

        # The other vE are just empty list
        for ve in self.list_ve:
            if ve not in pend_list.keys():
                pend_list[ve] = []

        return pend_list

    def reset_routing_msg(self):

        empty_list = dict()

        for ve in self.list_ve:
            empty_list[ve] = list()

        return empty_list

    def reset_simulation_state(self):

        self.received_ready_ve_signal = []  # empty the list of ready signals from vEs
        self.received_ready_modules_signal = []  # empty the list of ready signals from modules
        self.list_remaining_ve = self.reset_pending_list()  # restore the pending list of vEs

        self.module_already_contacted_when_done = False
        self.v_pool_done = False
        self.reached_end_simu_step = False

    def process_simulation_message(self, raw_msg):
        """ NEED TO OVERLOAD the vEntity version because this vE receive message in an asynchronous way """

        sender, msg_payload = raw_msg
        self._process_incoming_event(sender, msg_payload)

    def _process_incoming_event(self, sender, msg):
        """ The coordinator receives a new message
         It has to check its validity.
         Return true if the received signal is meant for signaling the end of activity"""

        # The received message is coming from the manager for coordination purpose

        if VE_VCOORD_MSG_TYPE_KEY in msg:

            # Coordination signal
            if msg[VE_VCOORD_MSG_TYPE_KEY] == VE_VCOORD_MSG_TYPE_READY_SIGNAL:
                if sender in self.list_ve and sender not in self.received_ready_ve_signal:  # from the vPool
                    self.received_ready_ve_signal.append(sender)
                    self.remove_from_pending_list(msg_sender=sender)
                elif sender in self.list_modules and sender not in self.received_ready_modules_signal:  # from a module
                    self.received_ready_modules_signal.append(sender)

            # Storing the message to be sent afterwards
            elif msg[VE_VCOORD_MSG_TYPE_KEY] == VE_VCOORD_MSG_TYPE_ROUTING:
                # Store it
                rec = msg[VE_VCOORD_ROUTING_TYPE_MSG_CONTENT][VE_VCOORD_ROUTING_TYPE_MSG_RECEIVER]
                payload = msg[VE_VCOORD_ROUTING_TYPE_MSG_CONTENT][VE_VCOORD_ROUTING_TYPE_MSG_PAYLOAD]
                new_msg = {VE_VCOORD_ROUTING_TYPE_MSG_SENDER: sender,
                            VE_VCOORD_ROUTING_TYPE_MSG_PAYLOAD: payload}

                if rec in self.routing_msg:
                    self.routing_msg[rec].append(new_msg)
                else:
                    self.routing_msg[rec] = [new_msg]

                # NB: don't remove this dependency from the list because one sender might send multiple events

    def remove_from_pending_list(self, msg_sender, msg_receiver=None):

        # Either removing for all the remaining keys or a specific one
        reciver_list = []
        if msg_receiver is None: # Remove the sender from all the lists
            reciver_list = self.list_remaining_ve.keys()
        elif msg_receiver in self.list_remaining_ve.keys():
            reciver_list = [msg_receiver]

        # Actually remove the elements
        for rec in reciver_list:
            if msg_sender in self.list_remaining_ve[rec]:
                self.list_remaining_ve[rec].remove(msg_sender)

    def _update_model_state(self):
        """UPDATE raw data and LINK the output values to the raw data"""

        # This means that all the virtual entities participating to the simulation have finished their job
        if len(self.received_ready_ve_signal) >= len(self.list_ve):
            self.v_pool_done = True
        else:
            self.v_pool_done = False

        # This means that all the actors of the simulation have finished their simulation
        if self.v_pool_done and (len(self.received_ready_modules_signal) >= len(self.list_modules)):
            self.reached_end_simu_step = True
        else:
            self.reached_end_simu_step = False

    def _update_current_time(self):
        if self.reached_end_simu_step:
            super(vCoordinator, self)._update_current_time()

    def _create_send_event(self):
        """ TO BE DESCRIBED """

        if self.reached_end_simu_step:

            # Reset the simulation state
            self.reset_simulation_state()

        if not self.v_pool_done:  # some message might still be sent
            simu_parameters = None  # TODO: simulation parameters

            for waiting_ve in self.list_remaining_ve.keys():
                if self.list_remaining_ve[waiting_ve] == []:  # this vE doesn't wait anymore !
                    rout_msg = []
                    if waiting_ve in self.routing_msg:
                        rout_msg = self.routing_msg[waiting_ve]
                        del self.routing_msg[waiting_ve]

                    ve_msg = {VE_VCOORD_MSG_TYPE_KEY: SIMU_COORD_MSG_TYPE,
                              VE_VCOORD_ROUTING_TYPE_MSG_CONTENT: rout_msg,
                              VE_VCOORD_SIMU_PARAM_KEY: simu_parameters}
                    self.send_event(receiver=waiting_ve, msg=json.dumps(ve_msg))

                    # Clear the buffer and remove this element from the pending list
                    del self.list_remaining_ve[waiting_ve]

        else:  # all the READY signals have been received from the vPOOL
            if not self.module_already_contacted_when_done:
                simu_parameters = None

                msg_to_send = {VE_VCOORD_MSG_TYPE_KEY: SIMU_COORD_MSG_TYPE, VE_VCOORD_SIMU_PARAM_KEY: simu_parameters}
                for mod in self.list_modules:
                    self.send_event(receiver=mod, msg=json.dumps(msg_to_send))

                self.module_already_contacted_when_done = True

    def _startup_seq(self, msg):
        super(vCoordinator, self)._startup_seq(msg)

        ###### START THE SIMULATION #####

        self._create_send_event()

    def find_parent(self, k, v, already_visited):
        d = self.list_dependency
        # Avoid cycling
        if k in already_visited or k not in d.keys():
            return []
        else:
            already_visited.append(k)

        # If value "v" is in the list of parents of "k", return k
        ret = []
        list_parent = d[k]
        if v in list_parent:
            ret = [k]
            print(k)

        # Ask recursively the parents
        for p in list_parent:
            if p != v:
                ret += self.find_parent(p, v, already_visited)

        return ret

    def remove_dependency_loop(self):
        """
        For a dependency dictionary, remove the possible loop in the graph
        :param d:
        :return: d without the loops
        """
        d = self.list_dependency

        for k in d.keys():  # all the vE
            for k_dep in d[k]:  # the list of vE the vE "k" depends on
                found_loop_pointer = self.find_parent(k_dep, k, [])  # find all the vE that points to "k"
                for k_parent in found_loop_pointer:
                    d[k_parent].remove(k)  # remove the link that create the loop
                    if k_parent in d[k]:  # if it's a double link, completely remove it
                        d[k].remove(k_parent)

        return d

