__author__ = 'lilis'
from conf_general import PROJECT_DIR, LOGGER_LVL

STATES_FILE = PROJECT_DIR + '/virtualization/vMiddlewareState.bin'  #The file to save the state of all vEntities

###  EMULATION IN R-T OR SIMULATION?  ###
SIMULATION_MODE_ON = True

## Simulation parameters and vCoordinator keys
if SIMULATION_MODE_ON:
    SIMULATION_TIME_STEP = 5*60  # 60s simulation step
    SIMULATION_DURATION = 7*24*60*60  # 1 hour simulation
    SIMULATION_CLOSED_SYSTEM = False  # Allow external modules to interact ?
    SPECIAL_ENTITY_ID_SIMULATION_COORD = 0  #TODO Special ID for the coordinator entity

## Type of received messages
VE_VCOORD_MSG_TYPE_KEY = "MSG_TYPE"
VE_VCOORD_MSG_TYPE_ROUTING = "coord_simu_routing" # this type of message is received from the vManager to indicate it routed a message coming from a vEntity
VE_VCOORD_MSG_TYPE_READY_SIGNAL = "coord_simu_ready" # this type of message is received from a virtual entity directly to the coordinator to indicate it's done

## Concerning forward type of message, list of possible keys
VE_VCOORD_ROUTING_TYPE_MSG_CONTENT = "CONTENT"
VE_VCOORD_ROUTING_TYPE_MSG_PAYLOAD = "PAYLOAD"
VE_VCOORD_ROUTING_TYPE_MSG_RECEIVER = "RECEIVER_ID"  # ID of the virtual entity that should receive this message
VE_VCOORD_ROUTING_TYPE_MSG_SENDER = "SENDER_ID"  # ID of the virtual entity that should send this message
VE_VCOORD_SIMU_PARAM_KEY = 'SIMU_PARAM' #Simulation parameter key

STATE_WAIT_TIME = 30  #The time in seconds to wait for the pool of vEntities to deliver their state for save
COOPERATIVE_QUANTUM_TIME = 0.01  # The "quantum" time during which a busy vE will periodically sleep to let the others work
VE_DELAY_BEFORE_START = 5  # Directly after run(), each vE sleeps until it gets the signal to start
VE_DEFAULT_SLEEPING_TIME = 60

EVENT_DELAY_THRES = 500 #Threshold before issuing a warning, in msec
COMMAND_DELAY_THRES = 500 #Threshold before issuing a warning, in msec
CRITICAL_DELAY_THRES = 10000 #Critical threshold. The engine collapsed. Terminate it. in msec
WARNING_FREQ = 15 #The frequency of warnings (send also by mail), to frequently and can collapse the vMid since it doesnt have time to reply to vE events. in sec

PING_INTERVAL = 5 #Ping the pool every x seconds
MSG_RECEIVE_TIMEOUT = PING_INTERVAL #General timeout when receiving messages from vEntities, should be proportional to PING_INTERVAL otherwise it may starve the ping

VMID_DEBUG_VERBOSE = False  # [7, 8]  # id(s) of the vE(s) to track, 0 if for all, False if none
VMID_DEBUG_SIMU_VERBOSE = True
VMID_DEBUG_SIMU_PROGRESS_ACC = 10

USE_IPC = True
PUB_IPC_SOCKET = 'ipc://pubsocket_vmid_ve'
SUB_IPC_SOCKET = 'ipc://subsocket_vmid_ve'
PUB_IP_SOCKET = 'tcp://127.0.0.1'
SUB_IP_SOCKET = 'tcp://127.0.0.1'
SOCKET_SETTLE_TIME = 5

BROADCAST_ADDR = 999999
VMID_MANAGER_ADDR = 0
UPLINK_ADDR = -1

COMMAND_MSG_TYPE = 'Command'
EVENT_MSG_TYPE = 'Event' #The type of event (STATUS/MEASURE) is in the payload
STARTUP_MSG_TYPE = 'Start'
SHUTDOWN_MSG_TYPE = 'Shutdown'
SHUTDOWN_COMPLETED_MSG_TYPE = 'Shutdown_Complete'
STATESAVE_MSG_TYPE = 'State_Save'
PING_MSG_TYPE_REQ = 'Ping_Request'
PING_MSG_TYPE_REP = 'Ping_Reply'
vMID_DELAY_WARNING_MSG_TYPE = 'Event_Delay_Warning'
vE_DELAY_WARNING_MSG_TYPE = 'Command_Delay_Warning'
SIMU_COORD_MSG_TYPE = 'simulation_coordination'

#For uplink messages payload keys
EVENT_DELAY_MSG_TYPE = 'Event_Delay'
COMMAND_DELAY_MSG_TYPE = 'Command_Delay'
ROUNDTRIP_DELAY_MSG_TYPE = 'RoundTrip_Delay'
HEALTH_REPORT_MSG_TYPE = 'Health_Report'

# The maximum period between two events
VE_MAX_MESSAGE_PERIOD_THRESHOLD = 60  # max of 60 without any event of an entity

#For Health Report
HEALTH_ITERATIONS_KEY = 'NUM_ITERATIONS'
HEALTH_COMPUTATION_KEY = 'COMPUTATION_TIME'
HEALTH_SLEEPING_KEY = 'SLEEPING_TIME'

#For vEntities parameters fields: general keys
EXPANDED_PARAM_TYPES = ["user", "generator", "storage", "load", "sensor", "actuator"]  # BMS fields
VE_CLASS_KEY = 'ventity_class'  # BMS default field generation
VE_SIMU_PARAM = 'simu_param'  # everything concerning the simulation part of the vE
VE_MODEL = 'model'  # everything that concerns the functional part of the vE

VE_RECEIVE_TIMEOUT_KEY = 'receive_timeout_ms'  # BMS default field generation
VE_CHANGE_THRESHOLD_KEY = 'quantity_change_threshold'
VE_VE_SUBSCRIBED = 'subscribed_ve'

VE_OUTPUT_VARIABLES = "output_variables"

#vUser actions
ACTION_ON_VE_CHANGE_MODE = 'VE_CHANGE_MODE'
ACTION_ON_VE_T_START_DEFINE = 'VE_START_TIME'
VLOAD_MSG_STATE_KEY = 'State'
VLOAD_MSG_QUANTITY_KEY = 'Quantity'
VLOAD_MSG_PARAM_KEY = 'Param'

#For downlink messages command: TO BE ADAPTED
TYPE_ACTION_NEW_P = 'NEW_P'  # Useful ???
TYPE_ACTION_MODE = 'NEW_MODE'  # For changing mode/dimmer
TYPE_ACTION_START = 'DEF_START'

#Important configuration
V_LOGGER_LVL = LOGGER_LVL
VMID_MID_ID = 11 #11 for Master 10 for vBenchmark
VMIDDLEWARE_CONF_STATE = 'VE_TEST'  #one of VE_TEST, VBENCH, MASTER