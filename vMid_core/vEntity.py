__author__ = 'Georgios Lilis'
import json
import logging
import time
from abc import ABCMeta, abstractmethod

import zmq.green as zmq
from gevent import Greenlet
from gevent import sleep as green_sleep

from conf_virtualization import *
from health_check_module import update_min_max_sum_trio
from ve_util import statDistribution

class vEntity(Greenlet):
    """This is an abstract class of the vEntity classes"""

    __metaclass__ = ABCMeta
    @abstractmethod
    def __init__(self, pubsubsocketpair, param, ownid, entitieslogger):
        """The zmq_context is shared from the vMiddleware manager, pubsubsocketpair are the ones successfully binded by
        vMiddleware. subscription filters the messages to the correct vE ID, it is a STRING of 6 bytes, it is derived from the vEid.
        The numbers are padded by zeros from left ({0:06d})e.g. 000001. BROADCAST_ADDR is a broacast address received by all the vEs"""
        Greenlet.__init__(self)

        (pub_socket_vE, sub_socket_vE) = pubsubsocketpair
        zmq_context = zmq.Context()
        self.vE_vMid_PUBsocket = zmq_context.socket(zmq.PUB)
        self.vE_vMid_SUBsocket = zmq_context.socket(zmq.SUB)

        self.vE_vMid_PUBsocket.connect(pub_socket_vE)
        self.vE_vMid_SUBsocket.connect(sub_socket_vE)
        self.vE_vMid_SUBsocket_poller = zmq.Poller()
        self.vE_vMid_SUBsocket_poller.register(self.vE_vMid_SUBsocket, zmq.POLLIN)

        self.vE_vMid_SUBsocket.setsockopt(zmq.SUBSCRIBE, '{0:06d}'.format(ownid))
        self.vE_vMid_SUBsocket.setsockopt(zmq.SUBSCRIBE, '{0:06d}'.format(BROADCAST_ADDR))

        self.logger = entitieslogger

        #  Entity ID
        self.vEid = ownid

        #  Signal used to hold on the entity
        self.entity_started = False

        #  Health check data for assessing the state of the entity:
        #  'NUM_ITERATIONS': counter that increases each time the vEntity finishes a loop turn in its "while True", since last reset
        #  'COMPUTATION': time spent by the vE for computational purpose - min, max and sum since last reset
        #  'SLEEPING': time spent by the vE for sleeping - min, max and sum since last reset

        self.health = {}
        self.reset_health_check()

        ###
        ###### Simulation parameters ########
        ###

        simu_param = {}
        if VE_SIMU_PARAM in param:
            simu_param = param[VE_SIMU_PARAM]

        # Is the engine in emulation or simulation mode?
        if SIMULATION_MODE_ON:
            self.dt = SIMULATION_TIME_STEP
        else:
            #  Time to sleep at each iteration
            if VE_RECEIVE_TIMEOUT_KEY in simu_param:
                if type(simu_param[VE_RECEIVE_TIMEOUT_KEY]) is dict:  # normal distribution
                    type_dt = simu_param[VE_RECEIVE_TIMEOUT_KEY]['distri_type']
                    param = list(p/1000.0 for p in simu_param[VE_RECEIVE_TIMEOUT_KEY]['distri_param'])
                else:
                    type_dt = "UNIF"
                    param = [simu_param[VE_RECEIVE_TIMEOUT_KEY]/1000.0] * 2

                self.dt_distri = statDistribution(type_dt, param)
            else:
                self.dt_distri = statDistribution("UNIF", [VE_DEFAULT_SLEEPING_TIME] * 2)

            # The sleeping time for each vE comes from a distribution
            self.dt = self.dt_distri.generateValue()

        #  Output event frequency regulation
        self.change_threshold = {}
        self.change_accumulator = {}
        if VE_CHANGE_THRESHOLD_KEY in simu_param:
            thresholds = simu_param[VE_CHANGE_THRESHOLD_KEY]
            for k in thresholds:
                self.change_threshold[k] = thresholds[k]
                self.change_accumulator[k] = 0

        # Last message
        self.last_msg_timestamp = 0  # Last time a msg was sent

        # Current time (real or simulation)
        self.creation_time = time.time()  # reference time
        self.current_time = time.time()  # current time
        if SIMULATION_MODE_ON:
            self.creation_time = 0  # reference time
            self.current_time = 0  # current time in simu: starts from 0

        # Inter-function variables: var used to pass parameters from one function to another, should last only one
        # iteration
        self.inter_function_variable = dict()

    @staticmethod
    def intelli_sleep(timetosleep):
        """Intelligent sleeper, passes transparently the control to the next uThread.
        :param timetosleep: float number, given in seconds"""
        green_sleep(timetosleep)

    @staticmethod
    def quantized_sleep():
        """Quantized sleeper, passes transparently the control to the next uThread."""
        green_sleep(0) #with 0 we pass directly the control to next

    def infini_sleep(self):
        """Sleep the maximum amount of time, expected to be waken up by a message """
        return self.get_command(1000)

    @staticmethod
    def generate_command(type_action, setting):
        """
        Generates a command that any entity is supposed to understand
        :param type_action: TODO
        :param setting: TODO
        :return:
        """
        ret = dict()

        ret['MID_ID'] = VMID_MID_ID
        ret['REF'] = None
        ret['MSG_TYPE'] = 'STATUS'
        ret['PAYLOAD'] = dict()

        ret['PAYLOAD']['Type'] = type_action
        ret['PAYLOAD']['Setting'] = setting
        ret['PAYLOAD']['Time'] = time.time()

        return ret

    @staticmethod
    def isCommand(msg):
        #TODO
        return ('MID_ID' in msg and 'REF' in msg and 'PAYLOAD' in msg and 'MSG_TYPE' in msg)

    @staticmethod
    def isFromSameFamily(class_test, class_parent):
        """
        return TRUE if class_test is a child of class_parent or the class itself
        """
        return class_test is class_parent or issubclass(class_test, class_parent)

    def get_relative_time(self, p):
        """ According to the current time, it returns the current time within the frame 'p' """
        return (self.current_time) % p

    def send_event(self, receiver, msg):
        """Send a multipart message to the vMiddleware, other vEntity, or broadcasted
        :param receiver: the receiver of the msg, vE ID, VMID_MANAGER_ADDR or BROADCAST_ADDR.
        It supports also list of receivers.
        :param msg: the payload of the message
        :return always TRUE"""
        if not isinstance(receiver, list):
            self.vE_vMid_PUBsocket.send_multipart(['{0:06d}'.format(receiver), '{0:06d}'.format(self.vEid), '{0:.6f}'.format(time.time()), msg])
        else:
            for addr in receiver:
                self.vE_vMid_PUBsocket.send_multipart(['{0:06d}'.format(addr), '{0:06d}'.format(self.vEid), '{0:.6f}'.format(time.time()), msg])
        self.last_msg_timestamp = self.current_time

        return True

    def get_command(self, recv_timeout):
        """Receive multipart message from the vMiddleware manager. It implements and checks for shutdown and startup commands
        :param recv_timeout: max allowed blocking time to receive, in seconds
        :return recipient: the intended recipient of the message, vE ID or VMID_MANAGER_ADDR or BROADCAST_ADDR
        :return sender: the sender of the message, vE ID or VMID_MANAGER_ADDR if it is from the higher hierarchical elements
        :return timestamp: the timestamp of original message
        :return msg: the payload of the message"""
        recv_timeout *= 1000
        last_warning = 0
        while True:
            sleep_timer = time.time()
            socks = dict(self.vE_vMid_SUBsocket_poller.poll(timeout=recv_timeout))

            if socks.get(self.vE_vMid_SUBsocket) == zmq.POLLIN:
                (recipient, sender, timestamp, msg) = self.vE_vMid_SUBsocket.recv_multipart()
                timestamp = float(timestamp)
                recipient = int(recipient)
                sender = int(sender)
                command_delay_instant = max(time.time() - timestamp, 0)*1000
                if command_delay_instant > COMMAND_DELAY_THRES:
                    #Inform houston we have a problem. Probably too many messages to process that the zmq queue
                    #of this vE is full and messages are waiting. Maybe ask for get_command more frequently!
                    if time.time() - last_warning > WARNING_FREQ:
                        self.log(logging.WARNING, 'vE {0} received packet from {1:06d} with delay of: {2:.3f} ms'.format(recipient, sender, command_delay_instant))
                        self.send_event(receiver=VMID_MANAGER_ADDR, msg=json.dumps({'MSG_TYPE': vE_DELAY_WARNING_MSG_TYPE}))
                        last_warning = time.time()
                    if command_delay_instant > CRITICAL_DELAY_THRES:
                        self.log(logging.ERROR, 'Terminating the vE due to critical delay {0} ms'.format(command_delay_instant))
                        self._shutdown_seq()
                        self.kill()
                        return
                try:
                    msg = json.loads(msg)
                    if msg['MSG_TYPE'] == STARTUP_MSG_TYPE:
                        self.log(logging.DEBUG, 'Got start message')
                        self._startup_seq(msg['PAYLOAD'])
                        self.entity_started = True
                        return
                    elif msg['MSG_TYPE'] == SHUTDOWN_MSG_TYPE:
                        self.log(logging.DEBUG, 'Got termination message')
                        self._shutdown_seq()
                        self.kill()
                        return
                    elif msg['MSG_TYPE'] == PING_MSG_TYPE_REQ:
                        msg['MSG_TYPE'] = PING_MSG_TYPE_REP #Send it back as reply type
                        msg['START_TIME'] = timestamp
                        msg['HEALTH_CHECK'] = self.health
                        self.send_event(receiver=sender, msg=json.dumps(msg))
                        self.reset_health_check()  # reset the accumulated data
                        recv_timeout -= 1000*(time.time() - sleep_timer)  # In case the vE needs to sleep again after this message
                        continue #Just a ping attempt to get ohter message
                    elif msg['MSG_TYPE'] == vMID_DELAY_WARNING_MSG_TYPE:
                        self.log(logging.DEBUG, 'vMid advised a reduction in messages frequency')
                        return
                    elif msg['MSG_TYPE'] == COMMAND_MSG_TYPE or msg['MSG_TYPE'] == EVENT_MSG_TYPE:
                        return recipient, sender, timestamp, msg['PAYLOAD']
                    elif msg['MSG_TYPE'] == SIMU_COORD_MSG_TYPE:  ## Simulation mode ; coming from vCoord
                        return msg[VE_VCOORD_ROUTING_TYPE_MSG_CONTENT], msg[VE_VCOORD_SIMU_PARAM_KEY]
                    elif msg['MSG_TYPE'] == VE_VCOORD_MSG_TYPE_ROUTING or \
                                    msg['MSG_TYPE'] == VE_VCOORD_MSG_TYPE_READY_SIGNAL:  ## Simulation mode ; for vCoord
                        return sender, msg
                    else:
                        return None #Should be none otherwise the recipient may cause exception error trying to read the dict
                except ValueError:
                    self.log(logging.WARNING, 'Received from {0:06d} non valid json payload: "{1}"'.format(sender, msg))
                    return None
            else:
                return None

    def reset_health_check(self):
        """
        Reset the current health check information vector
        :return:
        """
        # trio of min, max and sum
        init_trio = (float("inf"), 0, 0)

        self.health = {HEALTH_ITERATIONS_KEY: 0,
                       HEALTH_COMPUTATION_KEY: tuple(init_trio),
                       HEALTH_SLEEPING_KEY: tuple(init_trio)}

    def update_health(self, health_item, timer=-1):
        """ Update the "health_item" of the vector "health"
        :param health_item: the key in "health" of the element to update
        :param timer: if "timer" is given, the update change consists in withdrawing "timer" to the current time ;
        otherwise, it increments the item value
        :return: /
        """

        if timer >= 0:  # dealing with time
            value = max(time.time() - timer, 0)
            self.health[health_item] = update_min_max_sum_trio(self.health[health_item], value)
        else:  # dealing with counters to increment
            self.health[health_item] += 1

    def log(self, loglevel, logmsg):
        """Intelligent and identified logging.
        :param loglevel: the logging level as defined by the module logging
        :param logmsg: the message to be logged
        """
        self.logger.log(loglevel, 'vE {0:06d}: {1}'.format(self.vEid, logmsg))

    def save_state(self, state):
        self.send_event(receiver=VMID_MANAGER_ADDR, msg=json.dumps({'MSG_TYPE' : STATESAVE_MSG_TYPE, 'PAYLOAD' : state}))

    def _run(self):
        # MANDATORY, waiting start command, _startup_seq will be called before leaving this loop.
        while not self.entity_started:
            self.log(logging.DEBUG, '(vEntity of ID {0:06d}) Waiting for the start signal'.format(self.vEid))
            self.log(logging.DEBUG, '(vEntity of ID {0:06d}) Waiting for the start signal'.format(self.vEid))
            self.get_command(recv_timeout=VE_DELAY_BEFORE_START)
        ##########################

        print('{0} received START signal, going to sleep ! '.format(self.vEid))

        while True:

            # Command getting block ######
            sleeping_timer = time.time()
            """ LISTEN WITH TIMEOUT """
            ret = self._sleep_and_listen()# Receive command, block
            self.update_health(HEALTH_SLEEPING_KEY, sleeping_timer)
            ##########################

            if not SIMULATION_MODE_ON:
                self.current_time = time.time()

            # COMPUTATION BLOCK ######
            computing_timer = time.time()
            """ PROCESS INCOMING MESSAGE """
            if ret:
                if not SIMULATION_MODE_ON:  # Emulation mode
                    recipient, sender, timestamp, msg_payload = ret
                    self._process_incoming_event(sender, msg_payload)
                else:  # Simulation mode
                    #TODO: faire quelqu chose avec les simu param
                    self.process_simulation_message(ret)

            """ UPDATE THE VARIABLES and STATES OF THE MODEL """
            self._update_model_state()

            """ ADAPT THE TIMEOUT FOR THE SLEEPING PROCESS """
            if not SIMULATION_MODE_ON:
                self._update_sleep_interval()
            else:
                self._update_current_time()  # update the general clock

            self.update_health(HEALTH_COMPUTATION_KEY, computing_timer)
            ##########################

            # SENDING BLOCK ######
            sending_timer = time.time()
            """ CHECK IF THERE IS A SIGNIFICANT CHANGE FOR THE MODEL VARIABLES """
            self._create_send_event()
            ##########################
            self.update_health(HEALTH_ITERATIONS_KEY)

            ## SIMULATION MODE
            if SIMULATION_MODE_ON:
                self.send_ready_signal()

            self._clear_inter_function_variable()

    ## Simulation specific case functions ##
    def send_ready_signal(self):
        #TODO: more parameters ?

        msg = {VE_VCOORD_MSG_TYPE_KEY: VE_VCOORD_MSG_TYPE_READY_SIGNAL,
               VE_VCOORD_ROUTING_TYPE_MSG_CONTENT: {}}
        self.send_event(receiver=VMID_MANAGER_ADDR, msg=json.dumps(msg))

    def process_simulation_message(self, raw_msg):
        """
        Process "raw_msg" which is a combination of multiple messages
        :param ret:
        :return:
        """
        list_of_msg, simu_param = raw_msg
        if type(list_of_msg) is not list:
            list_of_msg = list(list_of_msg)

        for m in list_of_msg:
            sender = m[VE_VCOORD_ROUTING_TYPE_MSG_SENDER]
            msg_raw = m[VE_VCOORD_ROUTING_TYPE_MSG_PAYLOAD]
            if msg_raw != {}:

                #TODO: impose a universal message ..
                payload = msg_raw
                if 'PAYLOAD' in msg_raw:
                    payload = msg_raw['PAYLOAD']

                self._process_incoming_event(sender, payload)

    def _sleep_and_listen(self):
        """Method for blocking to receive from the vMiddleware manager."""
        # The vEntity either :
        # - sleeps infinitely (simulation mode) until receiving a coordination msg (SIMULATION MODE)
        # - sleeps a given amount of time (EMULATION MODE)

        if SIMULATION_MODE_ON:
            return self.infini_sleep()
        else:
            return self.get_command(recv_timeout=self.dt)


    def _clear_inter_function_variable(self):
        for k in self.inter_function_variable:
            self.inter_function_variable[k] = None

    def _update_current_time(self):

        # Simulation purpose
        self.current_time = self.current_time + self.dt

    @abstractmethod
    def _process_incoming_event(self, sender, payload):
        """Abstract method of _process_incoming_event that need to be overloaded by the children class.
        :param sender: the sender of the payload
        :param payload: the actual payload
        :return /"""
        pass

    @abstractmethod
    def _update_model_state(self):
        """Abstract method of _update_model_state that need to be overloaded by the children class.
        USE vEntity.quantized_sleep() if computing and updating the model state takes too long.
        :param pr_msg: the processed message
        :return the new model variables"""
        pass

    def _update_sleep_interval(self):
        """ method of _update_sleep_interval that might to be overloaded by the children class. It calculates and sets
        the new self.dt """
        self.dt = max(self.dt_distri.generateValue(), 0)

    @abstractmethod
    def _create_send_event(self):
        """Abstract method of _create_send_event that need to be overloaded by the children class."""
        pass

    def _startup_seq(self, savedstate):
        if savedstate != '' and savedstate != {}:
            self.state = savedstate

    def _shutdown_seq(self):
        #MANDATORY, send the state as an object in mystate. After the return from this place the vE terminates.
        if hasattr(self, 'state'):
            self.save_state(state=self.state)
        else:
            self.save_state({})
