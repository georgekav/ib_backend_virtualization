__author__ = 'Georgios Lilis'
# Note: Remember to ``pip install pyzmq gevent_zeromq``

import os
import sys
import time

PROJECT_DIR = os.path.abspath(__file__ + "/../../../")
sys.path.append(os.path.join(PROJECT_DIR, 'config'))
from conf_general import CURRENT_IP, PUB_MSG_TYPE
from conf_openbms import OPENBMS_IP, OPENBMS_PORT
from conf_rtserver import PUB_ID_OTHER, RT_SERVER_IP, RT_SERVER_PORT
from conf_virtualization import *

import websocket, requests
import loggercolorizer
from multiprocessing import Process
import signal, platform, cPickle
from vEntity import *
from health_check_module import HealthCheck

import numpy
from middlewareutilities import get_middleware_details
from vEntities_manager import vMiddleware_pool
from vCoordinator import *
from threading import Thread, Lock

if VMIDDLEWARE_CONF_STATE == 'VE_TEST':
    from test_virtualization import *
    import matplotlib.pyplot as plt


########################################################
#vMiddleware Manager functions
########################################################
def shutdown_entities():
    """Save the state of all the vEntities."""
    logger.debug("Entering shutdown sequence")
    send_message(receiver=BROADCAST_ADDR, msg=json.dumps({'MSG_TYPE' : SHUTDOWN_MSG_TYPE}))

    #Collect states to save, terminate after STATE_WAIT_TIME seconds of inactivity
    end_time = time.time() + STATE_WAIT_TIME
    combined_vE_states = {}
    while time.time() < end_time:
        ret = get_message(recv_timeout=0.1) #Get the state of the vEntity
        if ret:
            end_time = time.time() + STATE_WAIT_TIME #Refresh it
            (recipient, sender, timestamp, msg) = ret
            try:
                msg = json.loads(msg)
                if msg['MSG_TYPE'] == STATESAVE_MSG_TYPE:
                    logger.debug('Saving state of the vEntity {0:06d}'.format(sender))
                    combined_vE_states['{0:06d}'.format(sender)] = msg['PAYLOAD']
                elif msg['MSG_TYPE'] == SHUTDOWN_COMPLETED_MSG_TYPE:
                    break
            except ValueError:
                logger.warning('Received from {0:06d} non valid json payload: "{1}"'.format(sender, msg))
    statefilehandler = open(STATES_FILE, 'wb', buffering=0)
    pickler = cPickle.Pickler(statefilehandler, cPickle.HIGHEST_PROTOCOL)
    pickler.dump(combined_vE_states)
    statefilehandler.close()

    logger.info('Closing the downlink and uplink ZMQ sockets')
    close_n_destroy_sockets()
    logger.info('Terminating vMiddleware manager')


def startup_entities(list_entities_ids):
    """Save the state of all the vEntities.
    :return combined_vE_states the dictionary of json strings keeping the state of each vEntity"""
    logger.debug('Entering startup sequence')
    try:
        statefilehandler = open(STATES_FILE, 'rb', buffering=0)
        unpickler = cPickle.Unpickler(statefilehandler)
        combined_vE_states = unpickler.load()
        statefilehandler.close()
        assert isinstance(combined_vE_states, dict)
        for vEid in list_entities_ids:
            if vEid in combined_vE_states.keys():
                send_message(receiver=vEid, msg=json.dumps({'MSG_TYPE' : STARTUP_MSG_TYPE, 'PAYLOAD' : combined_vE_states['{0:06d}'.format(vEid)]}))
            else:
                send_message(receiver=vEid, msg=json.dumps({'MSG_TYPE' : STARTUP_MSG_TYPE, 'PAYLOAD' : {}}))
    except (EOFError, AssertionError):
        logger.error('States file corrupted')  # The creation is done in save_buffer()
    except IOError:
        logger.info('States file doesn\'t exist, nothing to restore')
        send_message(receiver=BROADCAST_ADDR, msg=json.dumps({'MSG_TYPE' : STARTUP_MSG_TYPE, 'PAYLOAD' : {}}))


def close_n_destroy_sockets():
    if vMid_vE_SUBsocket:
        vMid_vE_SUBsocket.setsockopt(zmq.LINGER, 0)
        vMid_vE_SUBsocket.close()
    if vMid_vE_PUBsocket:
        vMid_vE_PUBsocket.setsockopt(zmq.LINGER, 0)
        vMid_vE_PUBsocket.close()
    if uplink_REPsocket:
        uplink_REPsocket.setsockopt(zmq.LINGER, 0)
        uplink_REPsocket.close()
    if uplink_PUBsocket:
        uplink_PUBsocket.setsockopt(zmq.LINGER, 0)
        uplink_PUBsocket.close()
    if zmq_context:
        zmq_context.destroy()


def exit_gracefully(_signo, _stack_frame):
    """Terminates the sockets gracefully without any left hanging open."""
    shutdown_entities()
    logger.debug('Closing sockets and exiting gracefully')
    close_n_destroy_sockets()
    logger.info('Terminating vMiddleware manager')
    sys.exit(0)


def send_message(receiver, msg, sender=VMID_MANAGER_ADDR, timestamp=None):
    """Send a multipart message to vEntities or uplink, sender is 0 (vMid manager)
    :param receiver: the receiver of the msg, vE ID or BROADCAST_ADDR. It supports also list of receivers.
    :param msg: the payload of the message
    :param sender: not required, the sender to be written in the message. Necessary for forwarder of messages.
    :param timestamp: when needed by the uplink, pass the timestamp
    :return always TRUE"""
    if not isinstance(receiver, list):
        receiver = [receiver]
    for addr in receiver:
        if addr == UPLINK_ADDR: #going uplink for regular middleware type events
            if sender == VMID_MANAGER_ADDR:
                msg_type = PUB_MSG_TYPE[5]
                msg['Ref'] = sender
            else: #uplink from vEntities. MEASURE type of Middleware message

                msg_type = msg['PAYLOAD']['Type']
                msg = msg['PAYLOAD']
                msg = {'Ref': sender, 'Value': None}
            tosendmsg = {'MID_ID': VMID_MID_ID,
                         'MSG_TYPE': msg_type,
                         'TIME': timestamp,
                         'PAYLOAD': msg}
            #uplink_PUBsocket.send_json(tosendmsg)
        else: #vMiddleware pool internal commands

            receiver = addr

            # In case of simulation mode, all the msg targeted to a virtual entity have to be routed to the coordinator
            if SIMULATION_MODE_ON and addr is not SPECIAL_ENTITY_ID_SIMULATION_COORD:
                msg = json.loads(msg)

                if msg["MSG_TYPE"] == COMMAND_MSG_TYPE or msg["MSG_TYPE"] == EVENT_MSG_TYPE:
                    rec = addr
                    receiver = SPECIAL_ENTITY_ID_SIMULATION_COORD
                    payload = msg
                    msg = {}
                    msg['MSG_TYPE'] = VE_VCOORD_MSG_TYPE_ROUTING
                    msg[VE_VCOORD_ROUTING_TYPE_MSG_CONTENT] = {VE_VCOORD_ROUTING_TYPE_MSG_RECEIVER: rec,
                                                               VE_VCOORD_ROUTING_TYPE_MSG_PAYLOAD: payload}

                msg = json.dumps(msg)

            vMid_vE_PUBsocket.send_multipart(['{0:06d}'.format(receiver), '{0:06d}'.format(sender), '{0:.6f}'.format(time.time()), msg])
    return True


def get_message(recv_timeout):
    """Receive multipart message from the vEntities
    :param recv_timeout: max allowed blocking time to receive, in seconds
    :return recipient: the intended recipient of the message, vE ID or VMID_MANAGER_ADDR or BROADCAST_ADDR
    :return sender: the sender of the message, vE ID
    :return timestamp: the timestamp of original message
    :return msg: the payload of the message"""
    recv_timeout *= 1000
    socks = dict(vMid_socket_poller.poll(timeout=recv_timeout))

    if socks.get(vMid_vE_SUBsocket) == zmq.POLLIN:
        (recipient, sender, timestamp, msg) = vMid_vE_SUBsocket.recv_multipart()
        return int(recipient), int(sender), float(timestamp), msg

    elif socks.get(uplink_REPsocket) == zmq.POLLIN:
        msg = uplink_REPsocket.recv_json() #the uplink middleware is sending json messages.
        #the return message should be in the same tuple as the rest. and the msg should have the COMMAND type.
    else:
        return None


def ping_vE(receiver, sizebytes=32):
    """Measures the roundtrip time and response of a message to an vE. The response should be check in the main loop, cause
    many message from other vE arrive in the save socket. Thus filtering is necessary.
    :param receiver: the receiver of the msg, vE ID
    :param sizebytes: the size of the payload to send in bytes.
    """
    #F is just a default byte
    pingmsg = ''.join('F' for _ in range(sizebytes))
    pingpayload = json.dumps({'MSG_TYPE': PING_MSG_TYPE_REQ, 'PAYLOAD': pingmsg})
    send_message(receiver=receiver, msg=pingpayload)


def vMiddleware_manager(pubsubsocketpair, list_entities_ids, internInteract_table):
    """The vMiddleware frontend managing the communication uplink downlink and between the vE"""
    global vMid_vE_SUBsocket, vMid_vE_PUBsocket, uplink_PUBsocket, uplink_REPsocket, vMid_socket_poller, zmq_context

    zmq_context = zmq.Context()

    #External socket from vMID to Middleware
    vmiddleware = get_middleware_details(midid=VMID_MID_ID) #Execution will stop if it fails to find openBMS
    if vmiddleware['middleware_type'] !='vMID':
        logger.error('Could not find the  vmiddleware in openBMS, verify the ID in conf_virtualization.py')
        sys.exit()
    ip = vmiddleware['IP']
    rep_port = vmiddleware['REP_port']
    pub_port = vmiddleware['PUB_port']
    logger.info('Starting the middleware "{0}" @ "{1}:{2}/{3}"'.format(vmiddleware['name'], ip, rep_port,pub_port))

    uplink_REPsocket = zmq_context.socket(zmq.REP)
    uplink_REPsocket.bind('tcp://' + ip + ':' + rep_port)
    uplink_PUBsocket = zmq_context.socket(zmq.PUB)
    uplink_PUBsocket.bind('tcp://' + ip + ':' + pub_port)

    #Internal sockets between vMID // vE
    (vMid_PUBsocket, vMid_SUBsocket) = pubsubsocketpair
    vMid_vE_SUBsocket = zmq_context.socket(zmq.SUB)
    vMid_vE_PUBsocket = zmq_context.socket(zmq.PUB)
    vMid_vE_PUBsocket.setsockopt(zmq.LINGER, -1)

    vMid_vE_SUBsocket.bind(vMid_SUBsocket)
    vMid_vE_PUBsocket.bind(vMid_PUBsocket)

    vMid_vE_SUBsocket.setsockopt(zmq.SUBSCRIBE, '')

    vMid_socket_poller = zmq.Poller()
    vMid_socket_poller.register(vMid_vE_SUBsocket, zmq.POLLIN)
    vMid_socket_poller.register(uplink_REPsocket, zmq.POLLIN)

    time.sleep(SOCKET_SETTLE_TIME)  # Necessary for the sockets to settle.
    if not list_entities_ids:
        logger.info('There are no vEntities in the pool. Terminating the vMiddleware manager')
        return True #terminate the vMiddleware manager

    # Get an incoming message from a vEntity
    startup_entities(list_entities_ids)

    counter = 1
    last_ping = 0
    last_warning = 0
    simulation_time = float("inf")
    current_simulation_time = 0
    if VMIDDLEWARE_CONF_STATE == 'VE_TEST':
        samples_num = TEST_VIRTUALIZATION_NB_SAMPLE
        simulation_time = SIMULATION_DURATION

        # Plot
        t_vect = list()
        p_load = list()
        p_stor = list()
        p_gen = list()
        e_stor = list()

    elif VMIDDLEWARE_CONF_STATE == 'VBENCH':
        samples_num = TEST_SAMPLES_NUM
    else:
        samples_num = float("inf")
    idle_time = 0 #time just waiting for incoming communication

    start_time = time.time()

    ## FUTURE CHANGE: included in the "health check module"
    command_delay_cum = 0 #To reach vE
    event_delay_cum = 0 #To arrive from
    roundtrip_delay_cum = 0
    command_delay_list=[]
    event_delay_list=[]
    roundtrip_delay_list=[]

    ## Initialize the table that will store the health information about the pool
    health_check_module = HealthCheck(list_entities_ids)

    if VMIDDLEWARE_CONF_STATE == 'VE_TEST':
        # TEST PURPOSE
        ems, ems_plot = init_ems(list_entities_ids)

    pool_initialized = False
    while True:

        # END OF THE TEST/SIMULATION
        if (VMIDDLEWARE_CONF_STATE == 'VBENCH' and counter >= samples_num) or \
                (SIMULATION_MODE_ON and current_simulation_time > simulation_time): #Ideally it should be a shutdown command from uplink

            if VMIDDLEWARE_CONF_STATE == 'VE_TEST':
                close_ems(ems_plot)

            num_pings = len(command_delay_list)
            num_events = len(event_delay_list)

            logger.info("Time to reach {0}: {1}s"
                        .format(counter, round(time.time()-start_time,3)))
            logger.info("Uptime {0}s, Idling {1}s, Available computational power: {2}%"
                        .format(round(time.time() - start_time, 3), round(idle_time, 3),
                                round(idle_time / (time.time() - start_time) * 100, 1)))

            if num_pings > 0 and num_events > 0:
                logger.info("Performed {0} health checks".format(num_pings))
                logger.info("Average Roundtrip : {0}ms || One-way (vMid > vE): {1}ms || One-way (vE > vMid): {2}ms"
                            .format(round(roundtrip_delay_cum/num_pings,3),round(command_delay_cum/num_pings,3),
                                    round(event_delay_cum/num_events,3))) #event_delay_cum is counted for all events thus divided by num_events

            if len(roundtrip_delay_list) > 0 and len(command_delay_list) > 0 and len(event_delay_list) > 0:
                logger.info("Max Roundtrip : {0}ms || One-way (vMid > vE): {1}ms || One-way (vE > vMid): {2}ms"
                            .format(round(max(roundtrip_delay_list),3),round(max(command_delay_list),3),
                                    round(max(event_delay_list),3)))

                logger.info("Min Roundtrip : {0}ms || One-way (vMid > vE): {1}ms || One-way (vE > vMid): {2}ms"
                            .format(round(min(roundtrip_delay_list),3),round(min(command_delay_list),3),
                                    round(min(event_delay_list),3)))
                if VMIDDLEWARE_CONF_STATE == 'VBENCH':
                    filelogger.info("Total Time {6}s || Num of Pings {4} || Num of Events {5} ---- Avg Roundtrip {0}ms ---- (vMid > vE) {1}ms ---- (vE > vMid) {2}ms ---- Capacity {3}%"
                            .format(round(roundtrip_delay_cum/num_pings,3), round(command_delay_cum/num_pings,3),
                                round(event_delay_cum/num_events,3), round(idle_time/(time.time()-start_time)*100,1), num_pings, num_events, round(time.time()-start_time,3)))

            logger.info(health_check_module)
            shutdown_entities()

            return True #Terminating the manager

        # Periodically ping the pool of virtual entities
        ve_to_ping = health_check_module.pick_ve()
        if ve_to_ping is not None:
            ping_vE(ve_to_ping)

        timer1 = time.time()
        ret = get_message(recv_timeout=MSG_RECEIVE_TIMEOUT)
        idle_time += max(time.time() - timer1, 0)

        # Process the message received from the vE pool
        if ret:
            (recipient, sender, timestamp, raw_msg) = ret

            event_delay_instant = max(time.time() - timestamp, 0) * 1000
            event_delay_cum += event_delay_instant
            event_delay_list.append(event_delay_instant)

            #TODO: to be moved when updating the health check table
            if event_delay_instant > EVENT_DELAY_THRES:
                #The vMid is taxed with tasks and messages. Ask ALL the vE to shut-up!
                #This is rare unless the vMid has much more tasks than router of messages and is slow to check the zmq
                #for messages.
                if time.time() - last_warning > WARNING_FREQ:
                    logger.warning('vMid received packet from {0:06d} with delay of: {1:.3f}'.format(sender, event_delay_instant))
                    send_message(receiver=BROADCAST_ADDR, msg=json.dumps({'MSG_TYPE': vMID_DELAY_WARNING_MSG_TYPE}))
                    last_warning = time.time()
                if event_delay_instant > CRITICAL_DELAY_THRES:
                    logger.error('Terminating the vMid due to critical delay {0}ms'.format(event_delay_instant))
                    sys.exit()
            try:
                if VMIDDLEWARE_CONF_STATE == 'VE_TEST':
                    #  DEBUG PURPOSE
                    if VMID_DEBUG_VERBOSE is not False:
                        if VMID_DEBUG_VERBOSE == 0 or sender in VMID_DEBUG_VERBOSE:
                            logging.info("vEntity {0} for {1}".format(sender, recipient))
                            logging.info(raw_msg)
                    ################

                msg = json.loads(raw_msg)

                if msg['MSG_TYPE'] == EVENT_MSG_TYPE or msg['MSG_TYPE'] == COMMAND_MSG_TYPE:

                    if VMID_DEBUG_VERBOSE and (sender in VMID_DEBUG_VERBOSE or recipient in VMID_DEBUG_VERBOSE):
                        print("@EVENT: from vE {0} - {1}".format(sender, msg))

                    if not SIMULATION_MODE_ON and VMIDDLEWARE_CONF_STATE == 'VE_TEST':
                        temporary_simple_control(ems, current_simulation_time, ems_plot)

                    if recipient != VMID_MANAGER_ADDR:  # from a vE to a vE
                        send_message(receiver=recipient, sender=sender, msg=raw_msg)
                        logger.debug('Forwarded the {0}th event with id: {1} to vEid: {2:06d}'.format(counter, msg, recipient))
                    else:

                        # An event for the UPLINK
                        send_message(receiver=UPLINK_ADDR, sender=sender, timestamp=timestamp, msg=msg)

                        logger.debug('Served the {0}th event with id: {1} from vEid: {2:06d}'.format(counter, msg, sender))
                        if VMIDDLEWARE_CONF_STATE == 'VE_TEST' and msg['MSG_TYPE'] == EVENT_MSG_TYPE:
                            ems.update_state(sender, msg['PAYLOAD'], simulation_time)

                        ### FORWARD MSG TO INTERESTED ENTITIES ###
                        if sender in internInteract_table.keys():
                            for ve_id in internInteract_table[sender]:
                                send_message(receiver=ve_id, sender=sender, msg=raw_msg)
                    counter += 1

                    # Health check statistic: this vE has sent a new message to the manager
                    health_check_module.update(sender)
                    #logger.info(health_check_module)  # TEST PURPOSE

                    #UPLINK COMMAND, TODO it is better to check the sender if it is an uplink message...Because vE may have commands
                    #for other vE or other physical middleware.
                elif (SIMULATION_MODE_ON and (msg['MSG_TYPE'] == SIMU_COORD_MSG_TYPE or
                                                      msg['MSG_TYPE'] == VE_VCOORD_MSG_TYPE_READY_SIGNAL)):

                    #print("@SIMU | to vE {0}: {1}".format(recipient, msg))
                    if sender == SPECIAL_ENTITY_ID_SIMULATION_COORD:  # coming from the vCoord

                        if recipient != UPLINK_ADDR:  # from the vCoord to a vE: just forward
                            send_message(receiver=recipient, sender=sender, timestamp=timestamp, msg=raw_msg)
                        else:
                            # From the vCoordinator to the modules! This means that the vPool simu step is over
                            if VMIDDLEWARE_CONF_STATE == 'VE_TEST':
                                temporary_simple_control(ems, current_simulation_time, ems_plot)
                                current_simulation_time += SIMULATION_TIME_STEP  # NORMALLY DON'T DO THAT IF MULTIPLE MODULES !!

                            if VMID_DEBUG_SIMU_VERBOSE:
                                progress_prev = int(float(current_simulation_time - SIMULATION_TIME_STEP)/simulation_time * 100)
                                progress = int(float(current_simulation_time)/simulation_time * 100)
                                if progress_prev-progress_prev%VMID_DEBUG_SIMU_PROGRESS_ACC != progress-progress%VMID_DEBUG_SIMU_PROGRESS_ACC:
                                    print("{0}% --- Simulation progress: {1}s".format(int(progress), current_simulation_time))

                    elif msg['MSG_TYPE'] == VE_VCOORD_MSG_TYPE_READY_SIGNAL:
                        # This message was sent by a vE for the vCoord, BUT the vEs don't have its adress..
                        # So forward this message to the vCoord
                        send_message(receiver=SPECIAL_ENTITY_ID_SIMULATION_COORD, sender=sender, timestamp=timestamp, msg=raw_msg)

                elif msg['MSG_TYPE'] == PING_MSG_TYPE_REP:
                    command_delay_instant = max(timestamp - msg['START_TIME'], 0) * 1000
                    command_delay_cum += command_delay_instant
                    command_delay_list.append(command_delay_instant)

                    roundtrip_delay_instant = max(time.time() - msg['START_TIME'], 0) * 1000
                    roundtrip_delay_cum += roundtrip_delay_instant
                    roundtrip_delay_list.append(roundtrip_delay_instant)

                    ### TO BE REMOVED ?
                    send_message(receiver=UPLINK_ADDR, sender=VMID_MANAGER_ADDR, timestamp=time.time(),
                                 msg={COMMAND_DELAY_MSG_TYPE: round(command_delay_instant, 1),
                                      EVENT_DELAY_MSG_TYPE: round(event_delay_instant, 1),
                                      ROUNDTRIP_DELAY_MSG_TYPE: round(roundtrip_delay_instant, 1),
                                      HEALTH_REPORT_MSG_TYPE: msg['HEALTH_CHECK']
                                      })

                    ### TO BE REMOVED ? Update the health status of the pool and monitor it. If need, send the messages it generated
                    # Health check statistic: this vE has sent a reply to a PING
                    delay_data = {COMMAND_DELAY_MSG_TYPE: command_delay_instant,
                                  EVENT_DELAY_MSG_TYPE: event_delay_instant,
                                  ROUNDTRIP_DELAY_MSG_TYPE: roundtrip_delay_instant}

                    # A new PING message, containing information about the pinged vE and from which delays are infered
                    # Update the health check module and collect the messages it'd like to send
                    health_msg = health_check_module.update(sender, msg['HEALTH_CHECK'], delay_data)
                    for item in health_msg:
                        (rec, msg) = item
                        send_message(receiver=rec, sender=VMID_MANAGER_ADDR, msg=msg)

                elif msg['MSG_TYPE'] == vE_DELAY_WARNING_MSG_TYPE:
                    logger.info('vMid received a complain for starved vE {0}'.format(sender))
                elif msg['MSG_TYPE'] == STATESAVE_MSG_TYPE:
                    logger.debug('Saving state of the vEntity {0:06d}'.format(sender))
                    combined_vE_states = {}
                    combined_vE_states['{0:06d}'.format(sender)] = msg['PAYLOAD']
                    statefilehandler = open(STATES_FILE, 'wb', buffering=0)
                    pickler = cPickle.Pickler(statefilehandler, cPickle.HIGHEST_PROTOCOL)
                    pickler.dump(combined_vE_states)
                    statefilehandler.close()
                elif msg['MSG_TYPE'] == SHUTDOWN_COMPLETED_MSG_TYPE: #well bullshit happen and for some reason vE pool finished without our command. Terminate
                    logger.warning('For some reason vE pool finished without vMid command')
                    logger.info('Closing the downlink and uplink ZMQ sockets')
                    close_n_destroy_sockets()
                    return True
                else:
                    logger.warning('Unknown message type received {0}'.format(msg))

            except ValueError:
                logger.warning('Received from {0:06d} non valid json payload: "{1}"'.format(sender, raw_msg))

##################################################
################## EMS ###########################
##################################################

def init_ems(list_entities_ids):

    ## Empty structure for entities state
    ems_struct = EMS_State()

    for e in EMS_ENTITY_MAP:

        if e not in list_entities_ids:
            continue

        path = EMS_ENTITY_MAP[e]
        v = None
        if EMS_State.EMS_ENTITY_LOAD in path:
            v = LoadEntityStruct()
        elif EMS_State.EMS_ENTITY_GENERATION in path:
            v = GenerationEntityStruct()
        elif EMS_State.EMS_ENTITY_STORAGE in path:
            v = StorageEntityStruct()

        ems_struct.insert(path, e, v)

    ems_state = {'time': [],
                 EMS_State.EMS_ENTITY_LOAD: [],
                 EMS_State.EMS_ENTITY_GENERATION: [],
                 EMS_State.EMS_ENTITY_STORAGE+'power': [],
                 EMS_State.EMS_ENTITY_STORAGE+'energy': [],
                 'grid': []}

    return ems_struct, ems_state


def temporary_simple_control(ems, t, ems_state = None, verbose = False):
    """ EMS EMULATION """

    ## 0) store the current state
    power_load, power_gen, [power_stor, energy_stor] = ems.getAggregateState()

    if ems_state is not None and type(ems_state) is dict:
        ems_state['time'].append(t)
        ems_state[EMS_State.EMS_ENTITY_LOAD].append(power_load)
        ems_state[EMS_State.EMS_ENTITY_GENERATION].append(power_gen)
        ems_state[EMS_State.EMS_ENTITY_STORAGE+'energy'].append(energy_stor)

    ## 1) store/pump the imbalance to the batteries
    list_storage = ems.getEntitiesIdList(EMS_State.EMS_ENTITY_STORAGE)
    if len(list_storage):
        imbalance_per_storage = (power_load + power_gen) / len(list_storage)

        if energy_stor > 2000/1000:
            power_stor = -imbalance_per_storage * len(list_storage)

        ems_state[EMS_State.EMS_ENTITY_STORAGE+'power'].append(power_stor)

        payload = vEntity.generate_command(TYPE_ACTION_NEW_P, -imbalance_per_storage)
        for ent_stor in list_storage:
            send_message(receiver=ent_stor, sender=UPLINK_ADDR, msg=json.dumps({'MSG_TYPE': COMMAND_MSG_TYPE, 'PAYLOAD': payload}))
    else:
        ems_state[EMS_State.EMS_ENTITY_STORAGE+'power'].append(0)

    if ems_state is not None and type(ems_state) is dict:
        ems_state['grid'].append(power_load+power_gen+power_stor)

    ## 2) control the deferrable loads
    list_def = ems.getEntitiesIdList([EMS_State.EMS_ENTITY_LOAD, EMS_State.EMS_ENTITY_LOAD_DEFERRABLE])

    for ent_def in list_def:
        o = ems.get(ent_def)
        if t > o.start_time:

            # Send the ON command
            setting = {VLOAD_MSG_STATE_KEY: o.state,
                       VLOAD_MSG_QUANTITY_KEY: o.param,
                       VLOAD_MSG_PARAM_KEY: None}

            payload = vEntity.generate_command(TYPE_ACTION_MODE, setting)
            msg = json.dumps({'MSG_TYPE': COMMAND_MSG_TYPE, 'PAYLOAD': payload})
            send_message(receiver=ent_def, sender=UPLINK_ADDR, msg=msg)

            o.start_time = float('inf')

    #### FINISH THE SIMU STEP ####
    msg = {VE_VCOORD_MSG_TYPE_KEY: VE_VCOORD_MSG_TYPE_READY_SIGNAL,
               VE_VCOORD_ROUTING_TYPE_MSG_CONTENT: {}}
    send_message(receiver=SPECIAL_ENTITY_ID_SIMULATION_COORD, sender=UPLINK_ADDR, msg=json.dumps(msg))


#TODO: keep the graph !
def close_ems(ems_plot):

    t = ems_plot['time']
    p_l = ems_plot[EMS_State.EMS_ENTITY_LOAD]
    p_g = ems_plot[EMS_State.EMS_ENTITY_GENERATION]
    p_s = ems_plot[EMS_State.EMS_ENTITY_STORAGE+'power']
    e_s = ems_plot[EMS_State.EMS_ENTITY_STORAGE+'energy']
    p_grid = ems_plot['grid']

    # Power
    plt.figure(1)
    plt.subplot(211)
    plt.plot(t, p_l, 'b-', t, p_g, 'g-', t, p_grid, 'k-')

    # Power & energy of storage
    plt.subplot(212)
    plt.plot(t, p_s, 'r-', t, e_s, 'r--')
    plt.show()

    input("Press Enter to continue...")

########################################################
# External Event Listener
########################################################

def on_open(ws):
    logger.debug('Connected to rtserver for sensor events')
    regmsg = {"PUB_ID": PUB_ID_OTHER, "MSG_TYPE": "WEB_MSG",
              "PAYLOAD": {"MSG_CLASS": "REG_VMIDDLEWARE", "VMIDDLEWARE_ID": VMID_MID_ID}}
    ws.send(json.dumps(regmsg))

def on_message(ws, raw_msg):
    proc_msg = json.loads(raw_msg)
    if proc_msg['MSG_TYPE'] == "WEB_MSG" and proc_msg['PAYLOAD']['STATUS'] == True:
        logger.info('Successfully register to rtserver for events of room {0}'.format(proc_msg['PUB_ID']))
    elif proc_msg['MSG_TYPE'] == "ZMQ_MSG":
        if proc_msg['ID'] in vEntities_extEvent_subscriptions:
            del proc_msg['PUB_ID']
            proc_msg['MSG_TYPE'] = EVENT_MSG_TYPE
            for vE_id in vEntities_extEvent_subscriptions[proc_msg['ID']]:
                _sendlock.acquire() #Required since many on_message may be called in parallel
                external_events_PUBsocket.send_multipart(['{0:06d}'.format(vE_id), '{0:06d}'.format(VMID_MANAGER_ADDR), '{0:.6f}'.format(time.time()), json.dumps(proc_msg)])
                _sendlock.release()

def on_close(ws):
    logger.debug('Disconnected from rtserver for sensor events')

def on_error(ws, error):
    logger.warning('Failed to connect to rtserver for sensor events due to: {0}'.format(error))

def vMiddleware_external_event_manager(vMid_SUBsocket, sensor_interest):
    global external_events_PUBsocket, _sendlock, vEntities_extEvent_subscriptions
    vEntities_extEvent_subscriptions = sensor_interest #global scope

    zmq_context = zmq.Context()
    external_events_PUBsocket = zmq_context.socket(zmq.PUB)
    external_events_PUBsocket.connect(vMid_SUBsocket)

    _sendlock = Lock()

    websocket.enableTrace(False)  # True if you need debug info
    ws = websocket.WebSocketApp("ws://{0}:{1}/websocket".format(RT_SERVER_IP, RT_SERVER_PORT),
                                on_open=on_open,
                                on_message=on_message,
                                on_close=on_close,
                                on_error=on_error)

    ws.run_forever()

########################################################
#Utilities
########################################################
def get_zmq_socket_pair():
    """Find the list of possible ports for the middleware manager for a flexible setup, thus we have dynamic port
    binding"""

    if USE_IPC and platform.system() == 'Linux':
        temp_pub_socket, temp_sub_socket = (PUB_IPC_SOCKET, SUB_IPC_SOCKET)
    else:
        zmq_context = zmq.Context()

        test_socket_pub = zmq_context.socket(zmq.PUB)
        test_socket_sub = zmq_context.socket(zmq.SUB)

        pub_port = test_socket_pub.bind_to_random_port(PUB_IP_SOCKET)
        sub_port = test_socket_sub.bind_to_random_port(SUB_IP_SOCKET)

        test_socket_pub.setsockopt(zmq.LINGER, 0)
        test_socket_sub.setsockopt(zmq.LINGER, 0)

        test_socket_pub.unbind(PUB_IP_SOCKET+":{0}".format(pub_port))
        test_socket_sub.unbind(SUB_IP_SOCKET+":{0}".format(sub_port))

        test_socket_pub.close()
        test_socket_sub.close()

        temp_pub_socket = PUB_IP_SOCKET+":{0}".format(pub_port)
        temp_sub_socket = SUB_IP_SOCKET+":{0}".format(sub_port)

    return temp_pub_socket, temp_sub_socket

def get_v_middleware_entities(v_midid):
    """Returns the vEntities of the given vMiddleware id
    :param v_midid: the id of the vMiddleware
    :return sensor_interest: the dictionary of the interest of the vEntities to external events
    :return vEntity_param: the model parameters for the simulation in the vEntities"""
    geturl = 'http://{0}:{1}/API/v_middleware_entities/{2}'.format(OPENBMS_IP,OPENBMS_PORT, v_midid)
    r = requests.get(geturl)

    if r.status_code == 200: # Otherwise it means openBMS doesnt work
        proc_msg = r.json()
        sensor_interest = {} #keys the sensor id, values the vE ids
        internal_ve_interest = {} #keys the sensor id, values the vE ids
        vEntity_param = {} #keys the vE id, values the vE simulation parameters
        vEntity_expanded_param = {}
        list_dependency = {}
        last_id = 0
        for ve in proc_msg:
            vEid = ve['pk']

            # "External" interest
            for sensor in ve['fields']['sensors_interest']:
                sen_id = sensor['pk']
                try:
                    sensor_interest[sen_id].append(vEid)
                except (KeyError, TypeError):
                    sensor_interest[sen_id] = [vEid]

            # "internal" interest
            for other_ve_id in ve['fields']['internal_interest']:
                try:
                    internal_ve_interest[other_ve_id].append(vEid)
                except (KeyError, TypeError):
                    internal_ve_interest[other_ve_id] = [vEid]

            vEntity_param[vEid] = json.loads(ve['fields']['simulation_parameters'])

            # Find the right field in the extended parameters
            vEntity_expanded_param[vEid] = {}
            for k in EXPANDED_PARAM_TYPES:
                if k in ve['fields'] and ve['fields'][k] is not None:
                    vEntity_expanded_param[vEid] = ve['fields'][k]
                    break
            last_id = max(last_id, vEid)

            # Check the dependency btw vUser and vLoad(s)
            if VE_VE_SUBSCRIBED in vEntity_param[vEid]:
                list_ve = vEntity_param[vEid][VE_VE_SUBSCRIBED]
                for vl in list_ve:
                    if vl not in list_dependency:  # not already controlled by another vUser
                        list_dependency[vl] = [vEid]
                    else:
                        list_dependency[vl].append(vEid)


        # In case of SIMULATION mode, the special COORDINATOR entity must be spawn
        if SIMULATION_MODE_ON:
            SPECIAL_ENTITY_ID_SIMULATION_COORD = last_id + 1
            coord_ve = vEntity_param.keys()

            # TODO: in the future, maybe multiple modules acting on the pool, need to differenciate !
            coord_ext_modules = None
            if not SIMULATION_CLOSED_SYSTEM:
                coord_ext_modules = [UPLINK_ADDR]

            # Update the dependency table with internal ve interest
            for ve in internal_ve_interest.keys():
                for v_dep in internal_ve_interest[ve]:
                    if v_dep not in list_dependency:
                        list_dependency[v_dep] = [ve]
                    else:
                        list_dependency[v_dep].append(ve)

            vEntity_param[SPECIAL_ENTITY_ID_SIMULATION_COORD] = {VE_CLASS_KEY: 'vCoordinator',
                                                                 VE_VCOORD_LIST_VENTITIES: coord_ve,
                                                                 VE_VCOORD_LIST_EXT_MODULES: coord_ext_modules,
                                                                 VE_VCOORD_LIST_DEPENDENCY: list_dependency}

            vEntity_expanded_param[SPECIAL_ENTITY_ID_SIMULATION_COORD] = {}

        return sensor_interest, internal_ve_interest, vEntity_param, vEntity_expanded_param
    elif r.status_code == 404 and r.content:  # Server replied but not found the v_midid
        logging.error('Failed to find in openBMS the v_middleware: {0}'.format(v_midid))
        sys.exit()
    else:  # Server did not reply, maybe offline
        logging.error('Failed to connect to {0}:{1}, is openBMS running?'.format(OPENBMS_IP,OPENBMS_PORT))
        sys.exit()


if __name__ == '__main__':
    logger = loggercolorizer.getColorLogger(level=V_LOGGER_LVL, activate_email_logging=True,
                                            email_sender='vMiddaemon@{0}'.format(CURRENT_IP), send_email_level=logging.WARNING)
    global TEST_NUMBER_OF_VE, TEST_SAMPLES_NUM, RECV_TIMEOUT, PING_INTERVAL, MSG_RECEIVE_TIMEOUT

    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    if platform.system() == 'Linux':
        signal.signal(signal.SIGHUP, exit_gracefully)
        signal.signal(signal.SIGQUIT, exit_gracefully)

    sensor_interest, internal_ve_interest, vEntity_param, vEntity_expanded_param = get_v_middleware_entities(VMID_MID_ID)
    if VMIDDLEWARE_CONF_STATE == 'VBENCH':
        filelogger = loggercolorizer.getColorLogger(level=V_LOGGER_LVL, name='myfilelogger', filename='bench_res.log')

        # for benchmark only!!!!!!
        vMid_extEventsTread = None

        for recv_timeout in [100, 1000, 5000, 10000]:
            for ping_inteval in [1, 5, 10]:
                PING_INTERVAL = ping_inteval
                MSG_RECEIVE_TIMEOUT = PING_INTERVAL
                TEST_NUMBER_OF_VE = 50
                timewantedtorun = PING_INTERVAL*100*1000 #We want at least 100 ping messages
                sample_num = 150
                if sample_num < 50:
                    sample_num=50
                TEST_SAMPLES_NUM = TEST_NUMBER_OF_VE * sample_num
                filelogger.info("------------------------------------------------------------------------------------------------") #New line
                filelogger.info("TESTING >>> Num_vE {0} --- Recv_timeout {1}ms --- Samples {2} --- Ping_Interval {3}s"
                                .format(TEST_NUMBER_OF_VE, recv_timeout, sample_num, PING_INTERVAL))

                #For benchmark purposes, should be taken from openBMS
                vEntity_param = {}
                vEntity_expanded_param = {}
                for vEid in range(1, TEST_NUMBER_OF_VE + 1):
                    vEntity_param[vEid] = {
                                  "ventity_class":"vBenchmark",
                                  "receive_timeout_ms": recv_timeout,
                                  "vBenchmark_parameters":{
                                    "delays_ms":{
                                      "create_event":10,
                                      "model_setup":50,
                                      "startup":20,
                                      "update_model_state":100,
                                      "update_sleep_interval":10,
                                      "shutdown":40
                                    }
                                  }
                                }
                    vEntity_expanded_param[vEid] = {}

                #Create the sockets for the pool (inverse the pair)
                (sub_socket, pub_socket) = get_zmq_socket_pair()
                (vE_PUBsocket, vE_SUBsocket) = (sub_socket, pub_socket)  # Reverse them before sending to vEntities
                (vMid_PUBsocket, vMid_SUBsocket) = (pub_socket, sub_socket)  # Just rename

                # Launch the vEntities container process
                p = Process(target=vMiddleware_pool, args=((vE_PUBsocket, vE_SUBsocket), (vEntity_param, vEntity_expanded_param)))
                p.start()

                #for benchmark only!!!!!!
                if not vMid_extEventsTread:#for benchmark only!!!!!!#for benchmark only!!!!!!#for benchmark only!!!!!!#for benchmark only!!!!!!
                    # Begin the manager and the external event listener thread
                    vMid_extEventsTread = Thread(target=vMiddleware_external_event_manager, args=(vMid_SUBsocket, sensor_interest))

                    vMid_extEventsTread.daemon = True  # Die when the main does
                    vMid_extEventsTread.start()

                vMiddleware_manager((vMid_PUBsocket, vMid_SUBsocket), vEntity_param.keys(), internal_ve_interest)

    else:
        # Create the sockets for the pool (inverse the pair)
        (sub_socket, pub_socket) = get_zmq_socket_pair()
        (vE_PUBsocket, vE_SUBsocket) = (sub_socket, pub_socket)  # Reverse them before sending to vEntities
        (vMid_PUBsocket, vMid_SUBsocket) = (pub_socket, sub_socket)  # Just rename

        # Launch the vEntities container process
        p = Process(target=vMiddleware_pool, args=((vE_PUBsocket, vE_SUBsocket), (vEntity_param, vEntity_expanded_param)))
        p.start()

        # Begin the manager and the external event listener thread
        vMid_extEventsTread = Thread(target=vMiddleware_external_event_manager,
                                     args=(vMid_SUBsocket, sensor_interest))

        vMid_extEventsTread.daemon = True  # Die when the main does
        vMid_extEventsTread.start()

        if SIMULATION_MODE_ON:
            SPECIAL_ENTITY_ID_SIMULATION_COORD = max(vEntity_param.keys())
            print(SPECIAL_ENTITY_ID_SIMULATION_COORD)

        vMiddleware_manager((vMid_PUBsocket, vMid_SUBsocket), vEntity_param.keys(), internal_ve_interest)

