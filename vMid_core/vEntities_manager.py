__author__ = 'Georgios Lilis'

import zmq.green as zmq
import time
from gevent import joinall

import loggercolorizer
from conf_general import CURRENT_IP
# The class that might be instantiated

# Generic ones
from vGeneration import *
from vStorage import *
from vUser import *
from vSensor import *
from vActuator import *
from vCoordinator import *

# Subentities
from subEntities.vShiftable import *
from subEntities.vStorage_kibam import *
from subEntities.vPVpannel_uiModel import *

########################################################
#vMiddleware Backend functions (the vE container)
########################################################
def create_vEntities(pubsubsocketpair, vEntity_param, vEntity_expanded_param, logger):
    """Create the correct entities based on openBMS info.
    Very cool stuff, creates from a given string the correct Class instance to be used! Use with care"""
    ventities_list = []
    ve_act_list = {}  # link the vload_ID to its activity, for vUser purpose
    vUserList = []  # link the vUser_ID to its activity, for vUser purpose

    for vEid in vEntity_param.keys():
        try:
            # Find out the type of vEntity we're dealing with
            ventity_class_str = vEntity_param[vEid][VE_CLASS_KEY]
            logger.debug("ventity_class_str received: {0}".format(ventity_class_str))
            ventity_class_module = __import__(ventity_class_str) #filename MUST be the same with class name
            ventity_class = getattr(ventity_class_module, ventity_class_str)

            ## The virtual users needs the information of each vE it has in charge
            ## Therefore, they must be instantiated at the end, for the vE info to be collected
            if ventity_class is vUser:
                vUserList.append(vEid)
                continue

            # Create the valid parameter: a combination between simu and model parameters
            ext_param = None
            if vEid in vEntity_expanded_param:
                ext_param = vEntity_expanded_param[vEid]
            combined_param = translate_parameters(vEntity_param[vEid], ext_param, ventity_class)

            # Is the vE controllable by a vUser ? if so, get the LP_ACTIVITY such that the vUser can act on it
            if VE_PARAM_LP_MODEL in combined_param[VE_MODEL] and VE_PARAM_LP_ACTIVITY in combined_param[VE_MODEL][VE_PARAM_LP_MODEL]:
                ve_act_list[vEid] = combined_param[VE_MODEL][VE_PARAM_LP_MODEL].pop(VE_PARAM_LP_ACTIVITY, None)

            # Instantiate the vE based on the given class

            ventity = ventity_class(pubsubsocketpair, combined_param, vEid, logger)
            ventities_list.append(ventity)

            print("Instanciated: "+ventity_class_str)

            logger.debug("Class {0} added to the list of vEs to be instantiated!".format(ventity_class))

        except ImportError:
            logger.error('Wrong entity class name defined by the model for vEntity {0}, skipping this vE'.format(vEid))
        except AttributeError:
            logger.error('The class name in the module {0}.py does not match the filename'.format(ventity_class_str))

    ### Virtual Users instantiation
    # Parameter creation for the vUsers
    vu_combined_param = {}
    for vE in ve_act_list:  # for each load to be virtualized
        for vU in vUserList:  # for each virtual user
            if vU not in vu_combined_param:  # initialize the keys
                vu_combined_param[vU] = {VE_MODEL: {VE_USER_VE_ACTIVITIES: {}},
                                         VE_SIMU_PARAM: vEntity_param[vU]}

            # this vUser handles this vLoad: fill the right field
            if vE in vu_combined_param[vU][VE_SIMU_PARAM][VE_VE_SUBSCRIBED]:
                vu_combined_param[vU][VE_MODEL][VE_USER_VE_ACTIVITIES][vE] = ve_act_list[vE]

    # Instantiate the vUsers, now that they have enough information
    for vU in vUserList:
        ventities_list.append(vUser(pubsubsocketpair, vu_combined_param[vU], vU, logger))
        logger.debug("Class {0} added to the list of vEs to be instantiated!".format(vUser))

    return ventities_list

def vMiddleware_pool(pubsubsocketpair, vE_param):
    """The container and runner of all vEs"""
    (vEntity_param, vEntity_expanded_param) = vE_param
    logger = loggercolorizer.getColorLogger(level=V_LOGGER_LVL, activate_email_logging=True,
                                            email_sender='vMiddaemon@{0}'.format(CURRENT_IP), send_email_level=logging.WARNING,
                                            send_every=10*len(vEntity_param.keys()))

    (vE_PUBsocket, vE_SUBsocket) = pubsubsocketpair
    vPool_PUBsocket = vE_PUBsocket #Create one for the pool to notify the manager in case of need (like shutdown)

    #Fetch and create the entities
    entities_list = create_vEntities((vE_PUBsocket, vE_SUBsocket), vEntity_param, vEntity_expanded_param, logger)

    for entity in entities_list:
        entity.start()

    logger.info('Initialized {0} vEs'.format(len(vEntity_param.keys())))
    joinall(entities_list)
    ###########################################################
    #PROCESS IS STALLED HERE UNTIL ALL VENTITIES ARE TERMINATED
    ###########################################################

    logging.info('All vE terminated')

    #Notify the vMid that all vE have terminated. No need to wait further.
    zmq_context = zmq.Context()
    out_events_socket = zmq_context.socket(zmq.PUB)
    out_events_socket.connect(vPool_PUBsocket)
    time.sleep(1)
    out_events_socket.send_multipart(['{0:06d}'.format(VMID_MANAGER_ADDR), '{0:06d}'.format(VMID_MANAGER_ADDR), '{0:.6f}'.format(time.time()),
                                      json.dumps({'MSG_TYPE' : SHUTDOWN_COMPLETED_MSG_TYPE})])
    out_events_socket.setsockopt(zmq.LINGER, 0)
    out_events_socket.close()
    zmq_context.destroy()

    return True #Returning means all uTread have finished

def translate_parameters(simu_param, linked_param, vE_type):
    """
    From the "simulation" field and the "physical entity parameters" field in the BMS, create a dictionary that is
    formatted to be given to the vEntity class.

    :param simu_param: the "simulation parameters" field associated to a virtual entity in the BMS
    :param linked_param: the fields of the BMS object linked this virtual entity
    :param vE_type: the vE class to be instantiated

    :return: the formatted parameters that each special vE can understand
    """
    ret = {VE_SIMU_PARAM: {}, VE_MODEL: {}}

    # The key that was used to identify the type of class is now useless
    simu_param.pop(VE_CLASS_KEY, None)
    ret[VE_SIMU_PARAM] = simu_param

    #### For each type of entity, format the correct model parameters ####
    if vEntity.isFromSameFamily(vE_type, vLoad):

        # Model parameters
        add_param = json.loads(linked_param['fields']['additional_parameters'])
        lp_param = dict()

        lp_param[VE_PARAM_LP_ACTIVITY] = add_param.pop(VE_PARAM_LP_ACTIVITY, None)
        lp_param[VE_PARAM_LP_STATES] = add_param.pop(VE_PARAM_LP_STATES, None)
        lp_param[VE_PARAM_LP_MODES] = add_param.pop(VE_PARAM_LP_MODES, None)
        lp_param[VE_PARAM_LP_SEQUENCES] = add_param.pop(VE_PARAM_LP_SEQUENCES, None)
        lp_param[VE_PARAM_LP_TRANSITION] = add_param.pop(VE_PARAM_LP_TRANSITION, None)

        lp = loadProfile(lp_param)  # load profile object creation
        param_struct = dict()

        param_struct[VE_PARAM_LP_ACTIVITY] = lp.activity  # for vUser purpose

        param_struct[VE_PARAM_LP_STATES] = lp.states
        param_struct[VE_PARAM_LP_MODES] = lp.modes
        param_struct[VE_PARAM_LP_SEQUENCES] = lp.modes_sequence

        ret[VE_MODEL] = {}
        ret[VE_MODEL][VE_PARAM_LP_MODEL] = param_struct

        # The rest that don't fall into the LP
        for k in add_param:
            ret[VE_MODEL][k] = add_param[k]

    elif vEntity.isFromSameFamily(vE_type, vStorage):

        # Model parameters
        ret[VE_MODEL] = {}
        ret[VE_MODEL][VE_BAT_GEN_PARAM] = {}
        ret[VE_MODEL][VE_BAT_MODEL_PARAM] = {}

        # General parameters from the hardcoded fields
        ret[VE_MODEL][VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_CAPA] = linked_param['fields'][VE_BAT_GEN_PARAM_CAPA]
        ret[VE_MODEL][VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_CYCLES] = linked_param['fields'][VE_BAT_GEN_PARAM_CYCLES]

        # Parameters than are encoded in the additionnal field
        add_param = json.loads(linked_param['fields']['additional_parameters'])
        ret[VE_MODEL][VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_CH_EFF] = add_param[VE_BAT_GEN_PARAM_CH_EFF]
        ret[VE_MODEL][VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_DISCH_EFF] = add_param[VE_BAT_GEN_PARAM_CH_EFF]
        ret[VE_MODEL][VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_MAX_P_CH] = add_param[VE_BAT_GEN_PARAM_MAX_P_CH]
        ret[VE_MODEL][VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_MAX_P_DISCH] = add_param[VE_BAT_GEN_PARAM_MAX_P_DISCH]
        ret[VE_MODEL][VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_SOC_INIT] = add_param[VE_BAT_GEN_PARAM_SOC_INIT]
        ret[VE_MODEL][VE_BAT_GEN_PARAM][VE_BAT_GEN_PARAM_LEAK] = add_param[VE_BAT_GEN_PARAM_LEAK]

        ret[VE_MODEL][VE_BAT_GEN_PARAM][VE_STORAGE_SLEEP_PARAM] = {}
        ret[VE_MODEL][VE_BAT_GEN_PARAM][VE_STORAGE_SLEEP_PARAM][VE_STORAGE_SLEEP_SLIDING_WINDOW] = add_param[VE_STORAGE_SLEEP_PARAM][VE_STORAGE_SLEEP_SLIDING_WINDOW]
        ret[VE_MODEL][VE_BAT_GEN_PARAM][VE_STORAGE_SLEEP_PARAM][VE_STORAGE_SLEEP_SLIDING_WINDOW_WEIGHT] = add_param[VE_STORAGE_SLEEP_PARAM][VE_STORAGE_SLEEP_SLIDING_WINDOW_WEIGHT]

        # Parameters for each specific sub-entity
        #if vEntity.isFromSameFamily(vE_type, vStorage_kibam):
        ret[VE_MODEL][VE_BAT_MODEL_PARAM][VE_BAT_MODEL_PARAM_CAPA_RATIO] = add_param[VE_BAT_MODEL_PARAM_CAPA_RATIO]
        ret[VE_MODEL][VE_BAT_MODEL_PARAM][VE_BAT_MODEL_PARAM_RATE_K] = add_param[VE_BAT_MODEL_PARAM_RATE_K]

    elif vEntity.isFromSameFamily(vE_type, vGeneration):

        # Model parameters
        ret[VE_MODEL] = {}
        ret[VE_MODEL][VE_GEN_REF_PARAM] = {}
        ret[VE_MODEL][VE_GEN_PV_PARAM] = {}

        add_param = json.loads(linked_param['fields']['additional_parameters'])

        #if vEntity.isFromSameFamily(vE_type, vPVpannel_uiModel):
        ret[VE_MODEL][VE_GEN_REF_PARAM][VE_GEN_REF_PARAM_IRR] = add_param[VE_GEN_REF_PARAM][VE_GEN_REF_PARAM_IRR]  # G normal cond
        ret[VE_MODEL][VE_GEN_REF_PARAM][VE_GEN_REF_PARAM_TEMP] = add_param[VE_GEN_REF_PARAM][VE_GEN_REF_PARAM_TEMP]  # Ta normal cond
        ret[VE_MODEL][VE_GEN_REF_PARAM][VE_GEN_REF_PARAM_TEMP_NTC] = add_param[VE_GEN_REF_PARAM][VE_GEN_REF_PARAM_TEMP_NTC]  # Tcell normal cond

        ret[VE_MODEL][VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_VOLTAGE] = add_param[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_VOLTAGE]
        ret[VE_MODEL][VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_CURRENT] = add_param[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_CURRENT]
        ret[VE_MODEL][VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_VOLTAGE_TEMP_COEFF] = add_param[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_VOLTAGE_TEMP_COEFF]
        ret[VE_MODEL][VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_CURRENT_TEMP_COEFF] = add_param[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_STD_CURRENT_TEMP_COEFF]
        ret[VE_MODEL][VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_NB_CELLS] = add_param[VE_GEN_PV_PARAM][VE_GEN_PV_PARAM_NB_CELLS]

    elif vEntity.isFromSameFamily(vE_type, vActuator):

        # Model parameters
        ret[VE_MODEL] = dict(simu_param)
        ret[VE_MODEL][VE_VACTUATOR_COMMAND_TYPE] = linked_param['fields'][VE_VACTUATOR_COMMAND_TYPE]
        ret[VE_MODEL][VE_PARAM_LP_MODEL] = {}

        # Maybe a user-driven activity?
        add_param = json.loads(linked_param['fields']['additional_parameters'])
        param = dict()
        param[VE_PARAM_LP_ACTIVITY] = add_param.pop(VE_PARAM_LP_ACTIVITY, None)
        lp = loadProfile(param)  # load profile object creation

        ret[VE_MODEL][VE_PARAM_LP_MODEL][VE_PARAM_LP_ACTIVITY] = lp.activity

    elif vEntity.isFromSameFamily(vE_type, vSensor):

        # Model parameters: also included in the "simulation" field
        ret[VE_MODEL] = simu_param

    elif vEntity.isFromSameFamily(vE_type, vCoordinator):

        # The coordinator need the list of actors
        ret[VE_MODEL][VE_VCOORD_LIST_VENTITIES] = simu_param[VE_VCOORD_LIST_VENTITIES]
        ret[VE_MODEL][VE_VCOORD_LIST_EXT_MODULES] = simu_param[VE_VCOORD_LIST_EXT_MODULES]
        ret[VE_MODEL][VE_VCOORD_LIST_DEPENDENCY] = simu_param[VE_VCOORD_LIST_DEPENDENCY]
    else:
        ret[VE_MODEL] = linked_param

    return ret