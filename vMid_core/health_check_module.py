__author__ = 'Olivier Van Cutsem'

import time, random, json
from conf_virtualization import BROADCAST_ADDR, vMID_DELAY_WARNING_MSG_TYPE, PING_INTERVAL, WARNING_FREQ
from conf_virtualization import HEALTH_ITERATIONS_KEY, HEALTH_COMPUTATION_KEY, HEALTH_SLEEPING_KEY
from conf_virtualization import EVENT_DELAY_MSG_TYPE, COMMAND_DELAY_MSG_TYPE, ROUNDTRIP_DELAY_MSG_TYPE
from conf_virtualization import CRITICAL_DELAY_THRES, EVENT_DELAY_THRES

# General information
HEALTH_CHECK_LAST_CHECK_KEY = 'last_ping'
HEALTH_CHECK_DELAY_KEY = 'delays'
HEALTH_CHECK_DELAY_NB_SAMPLES_KEY = 'delays_nb_samples'

# Info per vE
HEALTH_CHECK_START_KEY = 'data_start_time'
HEALTH_CHECK_LAST_TIME = 'data_last_time'

HEALTH_CHECK_NB_MSG_KEY = 'nb_msg_rec'
HEALTH_CHECK_NB_CHECK_KEY = 'nb_health_check'
HEALTH_CHECK_NB_ITER_KEY = 'nb_iterations'

HEALTH_CHECK_DURATION_KEY = 'timelaps'

# for average data
HEALTH_CHECK_MEAN = 'mean_data'
HEALTH_CHECK_CURRENT_VALUES = 'current_data'

HEALTH_CHECK_WINDOW = 'sliding_window'
HEALTH_CHECK_SLIDING_WINDOW_SIZE = 3

class HealthCheck:
    """
    The class HealthCheck aim at representing a module that store information about the "health" of the pool of virtual
    entities, as well as their interaction with their manager
    """
    blank_minmaxmean_trio = (float("inf"), 0, 0)

    def __init__(self, items_list):

        # Some timing information
        self.last_checkup = time.time()  # The last request for pinging the pool
        self.last_monitoring_msg = 0  # The last time a WARNING message was produced
        self.timespan = {'first_check': None, 'last_check': None}  # first and last time a PING msg was received

        # The table containing all the data about the communication delays
        self.delays = dict()

        # The table containing the "health status" of each vE
        self.ve_status = dict()

        # Filling the tables
        n_win = HEALTH_CHECK_SLIDING_WINDOW_SIZE
        self.delays[HEALTH_CHECK_DELAY_NB_SAMPLES_KEY] = 0  # total nb of samples
        self.delays[HEALTH_CHECK_DURATION_KEY] = 0  # total nb of samples
        self.delays[EVENT_DELAY_MSG_TYPE] = {HEALTH_CHECK_MEAN: tuple(HealthCheck.blank_minmaxmean_trio),
                                             HEALTH_CHECK_WINDOW: SlidingWindow(n_win)}  # delay of an upward event
        self.delays[COMMAND_DELAY_MSG_TYPE] = {HEALTH_CHECK_MEAN: tuple(HealthCheck.blank_minmaxmean_trio),
                                               HEALTH_CHECK_WINDOW: SlidingWindow(n_win)}  # delay of an downward command
        self.delays[ROUNDTRIP_DELAY_MSG_TYPE] = {HEALTH_CHECK_MEAN: tuple(HealthCheck.blank_minmaxmean_trio),
                                                 HEALTH_CHECK_WINDOW: SlidingWindow(n_win)}  # roundtrip delay of a ping message

        # INFORMATION PER V_ENTITY
        for ve_id in items_list:
            self.ve_status[ve_id] = dict()

            # Accumulative data
            self.ve_status[ve_id][HEALTH_CHECK_MEAN] = dict()
            self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_CHECK_START_KEY] = time.time()  # first check msg
            self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_CHECK_NB_MSG_KEY] = 0  # nb of messages sent by this vE
            self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_CHECK_NB_CHECK_KEY] = 0 # nb of health check applied to this vE
            self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_ITERATIONS_KEY] = 0  # total amount of iterations of the vE
            self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_SLEEPING_KEY] = tuple(HealthCheck.blank_minmaxmean_trio)  # stat about sleeping time
            self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_COMPUTATION_KEY] = tuple(HealthCheck.blank_minmaxmean_trio) # stat about computing time

            # Most recent data
            self.ve_status[ve_id][HEALTH_CHECK_CURRENT_VALUES] = dict()
            self.ve_status[ve_id][HEALTH_CHECK_CURRENT_VALUES][HEALTH_CHECK_LAST_TIME] = time.time()  # last check applied to this vE
            self.ve_status[ve_id][HEALTH_CHECK_CURRENT_VALUES][HEALTH_CHECK_NB_MSG_KEY] = 0  # nb of messages sent by this vE, since last check

            # Most recent data, within a window
            self.ve_status[ve_id][HEALTH_CHECK_WINDOW] = dict()
            self.ve_status[ve_id][HEALTH_CHECK_WINDOW][HEALTH_CHECK_NB_MSG_KEY] = SlidingWindow(n_win)
            self.ve_status[ve_id][HEALTH_CHECK_WINDOW][HEALTH_ITERATIONS_KEY] = SlidingWindow(n_win)
            self.ve_status[ve_id][HEALTH_CHECK_WINDOW][HEALTH_SLEEPING_KEY] = SlidingWindow(n_win)
            self.ve_status[ve_id][HEALTH_CHECK_WINDOW][HEALTH_COMPUTATION_KEY] = SlidingWindow(n_win)

    def update(self, ve_id, health_data=None, delay_data=None):
        """
        Upon reception of a report, update the table containing the statistics of the pool

        :param ve_id: the entity id responsible of the call of this method
        :param health_data: new data about the vE, from a PING message
        :param delay_data: new data about the pool delays, from a PING message
        :return: a set of messages aimed to monitor the pool
        """

        if health_data is not None and delay_data is not None:  # new health check report !

            # Global timing
            if self.timespan['first_check'] is None:
                self.timespan['first_check'] = time.time()

            self.timespan['last_check'] = time.time()

            ### Update the delays of the pools

            self.delays[HEALTH_CHECK_DELAY_NB_SAMPLES_KEY] += 1

            self.delays[EVENT_DELAY_MSG_TYPE][HEALTH_CHECK_MEAN] = \
                update_min_max_sum_trio(self.delays[EVENT_DELAY_MSG_TYPE][HEALTH_CHECK_MEAN], delay_data[EVENT_DELAY_MSG_TYPE])
            self.delays[EVENT_DELAY_MSG_TYPE][HEALTH_CHECK_WINDOW].append(delay_data[EVENT_DELAY_MSG_TYPE])

            self.delays[COMMAND_DELAY_MSG_TYPE][HEALTH_CHECK_MEAN] = \
                update_min_max_sum_trio(self.delays[COMMAND_DELAY_MSG_TYPE][HEALTH_CHECK_MEAN], delay_data[COMMAND_DELAY_MSG_TYPE])
            self.delays[COMMAND_DELAY_MSG_TYPE][HEALTH_CHECK_WINDOW].append(delay_data[EVENT_DELAY_MSG_TYPE])

            self.delays[ROUNDTRIP_DELAY_MSG_TYPE][HEALTH_CHECK_MEAN] = \
                update_min_max_sum_trio(self.delays[ROUNDTRIP_DELAY_MSG_TYPE][HEALTH_CHECK_MEAN], delay_data[ROUNDTRIP_DELAY_MSG_TYPE])
            self.delays[COMMAND_DELAY_MSG_TYPE][HEALTH_CHECK_WINDOW].append(delay_data[EVENT_DELAY_MSG_TYPE])

            ### Update the table corresponding to the vE with the current data

            self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_CHECK_NB_MSG_KEY] += self.ve_status[ve_id][HEALTH_CHECK_CURRENT_VALUES][HEALTH_CHECK_NB_MSG_KEY]
            self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_CHECK_NB_CHECK_KEY] += 1

            # time elapsed since last health check

            (sleep_min, sleep_max, sleep_sum) = health_data[HEALTH_SLEEPING_KEY]
            (comp_min, comp_max, comp_sum) = health_data[HEALTH_COMPUTATION_KEY]
            dt = sleep_sum+comp_sum  # the total timespan for the vE measures
            if health_data[HEALTH_ITERATIONS_KEY] == 0 or dt == 0:  # no new data ..
                return {}

            # not very elegant but avoid bootstrap problem

            # nb iterations per time unit
            self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_ITERATIONS_KEY] += health_data[HEALTH_ITERATIONS_KEY]
            self.ve_status[ve_id][HEALTH_CHECK_WINDOW][HEALTH_ITERATIONS_KEY].append(health_data[HEALTH_ITERATIONS_KEY]/dt)

            # nb message per time unit
            msg_sum = self.ve_status[ve_id][HEALTH_CHECK_CURRENT_VALUES][HEALTH_CHECK_NB_MSG_KEY]
            self.ve_status[ve_id][HEALTH_CHECK_WINDOW][HEALTH_CHECK_NB_MSG_KEY].append(msg_sum/dt)

            # average % of sleeping
            self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_SLEEPING_KEY] = \
                update_min_max_sum_trio(health_data[HEALTH_SLEEPING_KEY], self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_SLEEPING_KEY])
            self.ve_status[ve_id][HEALTH_CHECK_WINDOW][HEALTH_SLEEPING_KEY].append(sleep_sum/dt)

            # average % of computation
            self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_COMPUTATION_KEY] = \
                update_min_max_sum_trio(health_data[HEALTH_COMPUTATION_KEY], self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_COMPUTATION_KEY])
            self.ve_status[ve_id][HEALTH_CHECK_WINDOW][HEALTH_COMPUTATION_KEY].append(comp_sum/dt)

            ### Reset current data
            self.ve_status[ve_id][HEALTH_CHECK_CURRENT_VALUES][HEALTH_CHECK_LAST_TIME] = time.time()
            self.ve_status[ve_id][HEALTH_CHECK_CURRENT_VALUES][HEALTH_CHECK_NB_MSG_KEY] = 0

            # After updating the state of the pool, monitor it: check the delays and the computation time of the vE
            return self.monitor_pool()

        else:  # the vE of interest has sent a new msg
            self.ve_status[ve_id][HEALTH_CHECK_CURRENT_VALUES][HEALTH_CHECK_NB_MSG_KEY] += 1

    def monitor_pool(self):
        """
        From the health check table that holds the information about the pool, check its state and generate messages
        to be sent if needed.
        return: a list of messages [(rec, msg), ...] to be sent down to the pool
        """
        NB_MSG_PER_SEC_THRES = 1
        vMID_DELAY_WARNING_AMOUNT_OF_MSG = "amount_of_msg_sent"
        #TODO: more accurate monitoring

        messages = []

        recent_delay = self.delays[EVENT_DELAY_MSG_TYPE][HEALTH_CHECK_WINDOW].mean()
        if recent_delay > EVENT_DELAY_THRES and time.time() - self.last_monitoring_msg > WARNING_FREQ:

            if recent_delay > CRITICAL_DELAY_THRES:
                messages.append((BROADCAST_ADDR, json.dumps({'MSG_TYPE': vMID_DELAY_WARNING_MSG_TYPE})))
            else:
                #TODO: target specific vE, based on the granular knowledge
                for ve in self.ve_status:
                    if self.ve_status[ve][HEALTH_CHECK_WINDOW][HEALTH_CHECK_NB_MSG_KEY].mean() > NB_MSG_PER_SEC_THRES:
                        messages.append((ve, json.dumps({'MSG_TYPE': vMID_DELAY_WARNING_MSG_TYPE,
                                                         'Quantity': vMID_DELAY_WARNING_AMOUNT_OF_MSG})))

        if messages is not {}:
            self.last_monitoring_msg = time.time()

        return messages

    def pick_ve(self):
        """
        Pick a vE to be pinged and assumes the message is sent directly after
        """
        if time.time() - self.last_checkup > PING_INTERVAL: #Every x second them with a ping
            list_ve = self.ve_status.keys()
            receiver = random.choice(list_ve)
            self.last_checkup = time.time()
            return receiver
        else:
            return None

    def __str__(self):

        ret = ""

        if self.timespan['first_check'] is None:
            return "NO DATA"

        # delays info
        nb_samples = self.delays[HEALTH_CHECK_DELAY_NB_SAMPLES_KEY]

        (event_min, event_max, event_sum) = self.delays[EVENT_DELAY_MSG_TYPE][HEALTH_CHECK_MEAN]
        (command_min, command_max, command_sum) = self.delays[COMMAND_DELAY_MSG_TYPE][HEALTH_CHECK_MEAN]
        (roundtrip_min, roundtrip_max, roundtrip_sum) = self.delays[ROUNDTRIP_DELAY_MSG_TYPE][HEALTH_CHECK_MEAN]

        ret += "### DELAYS - over {0} samples and {1} s: \n".format(self.delays[HEALTH_CHECK_DELAY_NB_SAMPLES_KEY],
                                                                round(self.timespan['last_check'] - self.timespan['first_check'],3))
        ret += "- EVENT: min = {0}ms, max = {1}ms and average = {2}ms\n".format(event_min, event_max, round(event_sum/nb_samples, 3))
        ret += "- COMMAND: min = {0}ms, max = {1}ms and average = {2}ms\n".format(command_min, command_max, round(command_sum/nb_samples, 3))
        ret += "- ROUNDTRIP: min = {0}ms, max = {1}ms and average = {2}ms\n".format(roundtrip_min, roundtrip_max, round(roundtrip_sum/nb_samples, 3))

        last_event_delay = round(self.delays[EVENT_DELAY_MSG_TYPE][HEALTH_CHECK_WINDOW].mean(),3)
        last_command_delay = round(self.delays[COMMAND_DELAY_MSG_TYPE][HEALTH_CHECK_WINDOW].mean(),3)
        last_roundtrip_delay = round(self.delays[ROUNDTRIP_DELAY_MSG_TYPE][HEALTH_CHECK_WINDOW].mean(),3)

        ret += "Last set of values: EVENT: {0}ms, COMMAND: {1} and ROUNDTRIP: {2}ms\n".format(last_event_delay, last_command_delay, last_roundtrip_delay)

        ret += "\n--------------------\n"

        # per vE info

        ret += "### STATUS OF EACH vENTITY: \n"

        for ve_id in self.ve_status:
            nb_health_check = self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_CHECK_NB_CHECK_KEY]
            nb_iter = self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_ITERATIONS_KEY]
            dt_ve = self.ve_status[ve_id][HEALTH_CHECK_CURRENT_VALUES][HEALTH_CHECK_LAST_TIME] - self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_CHECK_START_KEY]
            ret += "vE{0} - {1} health checks over {2}s: \n".format(ve_id, nb_health_check, round(dt_ve, 3))
            ret += " - Total: iterations = {0}, nb_msg = {1}, ".format(self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_ITERATIONS_KEY],
                                                                       self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_CHECK_NB_MSG_KEY])
            (sleep_min, sleep_max, sleep_sum) = self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_SLEEPING_KEY]
            if nb_iter == 0:
                nb_iter = 1
            ret += "idle = (min: {0}ms, max: {1}ms, mean: {2}ms),".format(round(sleep_min, 3), round(sleep_max, 3), round(sleep_sum/nb_iter, 3))
            (comp_min, comp_max, comp_sum) = self.ve_status[ve_id][HEALTH_CHECK_MEAN][HEALTH_COMPUTATION_KEY]
            ret += "busy = (min: {0}ms, max: {1}ms, mean: {2}ms) \n".format(round(comp_min, 3), round(comp_max, 3), round(comp_sum/nb_iter, 3))

            last_msg = round(self.ve_status[ve_id][HEALTH_CHECK_WINDOW][HEALTH_CHECK_NB_MSG_KEY].mean(), 3)
            last_iter = round(self.ve_status[ve_id][HEALTH_CHECK_WINDOW][HEALTH_ITERATIONS_KEY].mean(), 3)
            last_sleep = round(self.ve_status[ve_id][HEALTH_CHECK_WINDOW][HEALTH_SLEEPING_KEY].mean(), 3)
            last_comp = round(self.ve_status[ve_id][HEALTH_CHECK_WINDOW][HEALTH_COMPUTATION_KEY].mean(), 3)

            ret += " - Instantaneous values: iterations = {0}/s, nb_msg = {1}/s and CPU: (idle: {2}%, busy: {3}%) \n".format(last_iter,
                                                                                                                      last_msg,
                                                                                                                      last_sleep*100,
                                                                                                                      last_comp*100)

        return ret

############# SHOULD BE MOVED TO "auxiliary" folder #######################

def update_min_max_sum_trio(v1, v2):
    """
    From two tuples of (min, max, sum), computes the various mathematical methods
    :param v1: the first tuple
    :param v2: the second tuple
    :return:
    """
    if type(v1) is not tuple:
        if type(v1) is list:
            v1 = tuple(v1)
        else:
            v1 = (v1, v1, v1)

    if type(v2) is not tuple:
        if type(v2) is list:
            v2 = tuple(v2)
        else:
            v2 = (v2, v2, v2)

    (min1, max1, sum1) = v1
    (min2, max2, sum2) = v2

    return min(min1, min2), max(max1, max2), sum1+sum2


class SlidingWindow(list):
    """
    Class representing a "sliding window". It's basically a list of a fixed size, such that the appending of a new data
    implies the shifting of all the values, erasing the first one.
    """
    def __init__(self, length, init_value=0):
        super(SlidingWindow, self).__init__(list([init_value] * length))
        self._sum = init_value * length
        self._mean = init_value
        self._len = length

    def append(self, val):
        self._sum += (val - self.pop(0))
        super(SlidingWindow, self).append(val)
        self._mean = float(self._sum)/self._len

    def mean(self):
        return self._mean

    def __str__(self):
        return super(SlidingWindow, self).__str__()

