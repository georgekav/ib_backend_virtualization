__author__ = 'Olivier Van Cutsem'

import os, time
import shutil
from ems_state import EMS_State
from ems_state_entity import *
from matplotlib import pyplot as plt

# Define
TEST_VIRTUALIZATION_NB_SAMPLE = float("inf")
TEST_VIRTUALIZATION_SIM_TIME = 5*60  # 5 min

# vMid control support
LOAD_CLUSTER_ITEM = "loads"
STORAGE_CLUSTER_ITEM = "storage"
STOR_ENERGY_CLUSTER_ITEM = "storage_energy"
GENERATION_CLUSTER_ITEM = "generation"
GRID_CLUSTER_ITEM = "grid"

# TO BE REPLACED as the EMS connexion is established ####
# Keep track of the activity in term of power flows
temporary_cluster_power = {LOAD_CLUSTER_ITEM: 0, STORAGE_CLUSTER_ITEM: 0, GENERATION_CLUSTER_ITEM: 0, STOR_ENERGY_CLUSTER_ITEM: 0, GRID_CLUSTER_ITEM: 0}
temporary_cluster_power_granular = {LOAD_CLUSTER_ITEM: {}, STORAGE_CLUSTER_ITEM: {}, GENERATION_CLUSTER_ITEM: {}, STOR_ENERGY_CLUSTER_ITEM: {}}

#def loads
deferrable_loads_pending_list = {}

##### Log file name
start_time_test_virt = time.time()
FOLDER_GLOBAL = 'vWorld_' + time.strftime('%b-%d-%Y') + "/"
FILE_GLOBAL = 'global_power_flow' + '.csv'
FILE_GRANULAR_SUFFIXE = '_quantity_flow.csv'

list_files = {'global': None, 've': {}}

# Open files for data storage

def init_test_log():

    # Make dir and File object
    if os.path.isfile(FOLDER_GLOBAL+FILE_GLOBAL):
        shutil.rmtree(FOLDER_GLOBAL)

    os.makedirs(FOLDER_GLOBAL)
    os.makedirs(FOLDER_GLOBAL+LOAD_CLUSTER_ITEM+"/")
    os.makedirs(FOLDER_GLOBAL+STORAGE_CLUSTER_ITEM+"/")
    os.makedirs(FOLDER_GLOBAL+GENERATION_CLUSTER_ITEM+"/")

    f = open(FOLDER_GLOBAL+FILE_GLOBAL, 'a+')
    h = "time"
    for k in temporary_cluster_power.keys():
        h += "," + k
    f.write(h + "\n")

    list_files['global'] = f

def open_file(type_ve, ve_trigger):
    """
    :param ve_trigger: the vE that caused a new file to be created
    :return:
    """

    header = "time,power,energy\n"

    # if this file is already opened, skip it ..
    if ve_trigger not in list_files['ve']:
        file_name = FOLDER_GLOBAL + type_ve + "/" + str(ve_trigger) + FILE_GRANULAR_SUFFIXE
        list_files['ve'][ve_trigger] = open(file_name, 'a+')
        list_files['ve'][ve_trigger].write(header)


def store_change(veID_trigger, ve_type):
    """
    :param ve_trigger: the vE responsible for changing
    :return:
    """

    # the file of the vE
    if veID_trigger in list_files['ve']:
        new_data_ve = str(get_rel_time()) + "," + str(temporary_cluster_power_granular[ve_type][veID_trigger])

        # Only storage system have an "energy" field
        if ve_type == STORAGE_CLUSTER_ITEM:
            new_data_ve += "," + str(temporary_cluster_power_granular[STOR_ENERGY_CLUSTER_ITEM][veID_trigger])
        else:
            new_data_ve += ",0"

        list_files['ve'][veID_trigger].write(new_data_ve + "\n")

    # The global file
    new_data_global = str(get_rel_time())
    for k in temporary_cluster_power.keys():
        new_data_global += "," + str(temporary_cluster_power[k])

    list_files['global'].write(new_data_global + "\n")

def close_test_virtualization():
    # There is a file per vE
    for ve_id in list_files['ve']:
        list_files['ve'][ve_id].close()

    # There is a file for the global temporary_cluster_power
    list_files['global'].close()