__author__ = 'Olivier Van Cutsem'

from ems_state_entity import EMS_ENTITY_MAP
from conf_general import PUB_MSG_TYPE
import numpy as numpy

class CategoryTree(object):

    DICT_CREATION_LABEL_LIST = 'list-label'

    def __init__(self, category_struct=None):

        self.category_set = {}

        if type(category_struct) is dict:
            self.category_set = self.create_set(category_struct)

    def insert(self, path, entity_id, value=None):
        self.set_value(path, entity_id, value)

    def set(self, entity_id, v):
        [o, path] = self.find_in_set(entity_id, self.category_set)
        self.set_value(path, entity_id, v)

    def get(self, entity_id):
        [o, path] = self.find_in_set(entity_id, self.category_set)
        return o, path

    def getEntitiesValuesList(self, label):

        if label in self.category_set.keys():
            return self.list_of_item(self.category_set[label], True)
        else:
            return []

    def getEntitiesIdList(self, label):

        if label in self.category_set.keys():
            return self.list_of_item(self.category_set[label], False)
        else:
            return []

    ##################################
    ### Dictionary manipulation ###
    ###############################

    def create_set(self, cat_struct):
        """

        :param cat_struct: a dictionary describing the categories.
        Each key DICT_CREATION_LABEL_LIST indicates the sub-categories of each category
        :return: a dictionary ready to receive data in each category

        Example:
            cat_struct = {['cars' 'motorcycle' 'bikes'], 'cars': ['diesel' 'ev']}
            return: {'cars': {'diesel': {}, 'ev': {}}, 'motorcycle': {}, 'bikes': {}}
        """
        cat_dict = {}

        # if the label that describes the list of sub-cat is found, keep digging recursively
        if self.DICT_CREATION_LABEL_LIST in cat_struct:
            list_cat = cat_struct[self.DICT_CREATION_LABEL_LIST]  # list of the sub-categories
            for k in list_cat:
                cat_dict[k] = {}
                if k in cat_struct.keys():
                    cat_dict[k] = self.create_set(cat_struct[k])

        return cat_dict

    def find_in_set(self, id, set):

        if set is None:
            return None, []

        # Either it's a leaf, we check if it contains the value
        if self.isLeaf(set):
            if id in set:  # OK you're looking after me !
                return set[id], []
            else:
                return None, []

        # OR look to all of the children of the current category

        for k in set.keys():
            [obj, path] = self.find_in_set(id, set[k])  # OK we found the item in this category
            if obj is not None:
                return obj, (list(k) + path)

        return None, []

    def isLeaf(self, set):
        """
        A leaf is defined as a dictionary whose values are not dictionary and hence contains values
        :param set:
        :return:
        """

        l = set.values()

        if l is None or l == [] or type(l[0]) is not dict:
            return True
        else:
            return False

    def get_value(self, path):

        cur_dict = self.category_set

        for k in path:
            if k in cur_dict:
                cur_dict = cur_dict[k]
            else:
                return None

        return cur_dict

    def set_value(self, path, id, v):
        """
        :param path:
        :param id:
        :param v:
        :return: set[path][id] = v
        """
        cur_dict = self.category_set
        for k in path:
            if k in cur_dict:
                cur_dict = cur_dict[k]
            else:
                return None

        cur_dict[id] = v
        return v

    def list_of_item(self, set, val_select=True):

        # Either it's a leaf, just return the list of values
        if self.isLeaf(set):
            if val_select:
                return set.values()
            else:
                return set.keys()

        # OR dig into the children of each category
        l_item = []
        for k in set.keys():
            l_item += self.list_of_item(set[k], val_select)

        return l_item


class EMS_State(CategoryTree):

    # Level 1 entities types
    EMS_ENTITY_LOAD = 'L'
    EMS_ENTITY_STORAGE = 'S'
    EMS_ENTITY_GENERATION = 'G'

    # Level 'L' entities types
    EMS_ENTITY_LOAD_USER_DRIVEN = 'USER-DRIVEN'
    EMS_ENTITY_LOAD_DEFERRABLE = 'DEF'
    EMS_ENTITY_LOAD_THERM = 'THERM'

    def __init__(self):

        # The main categories of building entities
        main_list = [self.EMS_ENTITY_LOAD, self.EMS_ENTITY_STORAGE, self.EMS_ENTITY_GENERATION]

        # LOAD categories
        load_list = [self.EMS_ENTITY_LOAD_USER_DRIVEN, self.EMS_ENTITY_LOAD_DEFERRABLE, self.EMS_ENTITY_LOAD_THERM]
        ems_cat_struct = {self.DICT_CREATION_LABEL_LIST: main_list, self.EMS_ENTITY_LOAD: {self.DICT_CREATION_LABEL_LIST: load_list}}

        super(EMS_State, self).__init__(category_struct=ems_cat_struct)

    def getAggregateState(self):
        """
        return the current EMS state, in term of power consumption (L), power generation (G) and power/energy storage (Sp/Se)
        :return: [L, G, [Sp, Se]]
        """

        consumption_list = self.getEntitiesValuesList(self.EMS_ENTITY_LOAD)
        generation_list = self.getEntitiesValuesList(self.EMS_ENTITY_GENERATION)
        storage_list = self.getEntitiesValuesList(self.EMS_ENTITY_STORAGE)

        # Power
        p_l = 0
        for el in consumption_list:
            p_l += el.power

        p_g = 0
        for eg in generation_list:
            p_g += eg.power

        p_s = 0
        e_s = 0
        for es in storage_list:
            p_s += es.power
            e_s += es.getEnergy()

        return [p_l, p_g, [p_s, e_s]]

    def update_state(self, sender, received_event, current_time):

        # Link the entity to the type
        [obj, path] = self.get(sender)

        # Sender not part of the EMS process
        if obj is None and path == []:
            print("Couldn't find the EMS object linked to {0}".format(sender))
            return

        print("About to update EMS with: {0} in {1}".format(sender, path))
        if received_event['Type'] == PUB_MSG_TYPE[1]:
            quantity = received_event['Quantity']
            if self.EMS_ENTITY_LOAD in path or self.EMS_ENTITY_GENERATION in path:  # it's a load or a generator

                if 'P' in quantity:  # Power measurement
                    obj.power = quantity['P']

                if "Type" in quantity and quantity["Type"] == "start_time":  # deferrable loads
                    start_time = numpy.random.random_integers(int(round(current_time)), int(round(quantity["Value"])))
                    obj.schedule_new_start(start_time, quantity["state"], quantity["param"])

            elif self.EMS_ENTITY_STORAGE in path:  # it's a storage system
                if 'SOC' in quantity:
                    obj.soc = quantity['SOC']
                if 'P' in quantity:  # Power measurement
                    obj.power = quantity['P']
