__author__ = 'Olivier Van Cutsem'

### TEST PURPOSE ###
#TODO: remove it when EMS is a process by itself

EMS_ENTITY_MAP = {
    7: ['G'],
    8: ['S'],
    5: ['L', 'USER-DRIVEN'],
    9: ['L', 'USER-DRIVEN']
}

class LoadEntityStruct(object):

    PARAM_KEY_P_MAX = 'max-power'

    def __init__(self, param=None):

        # parameters
        self.p_max = float('inf')
        self.p_min = -float('inf')
        if param is not None:
            if self.PARAM_KEY_P_MAX in param:
                self.p_max = param[self.PARAM_KEY_P_MAX]
            if self.PARAM_KEY_P_MIN in param:
                self.p_min = param[self.PARAM_KEY_P_MIN]

        # state
        self.power = 0

class DeferrableLoadEntityStruct(LoadEntityStruct):

    def __init__(self, param=None):

        super(DeferrableLoadEntityStruct, self).__init__(param)

        self.start_time = float('inf')
        self.state = None
        self.param = None

    def schedule_new_start(self, start_time, state, param):
        self.start_time = start_time
        self.state = state
        self.param = param


class GenerationEntityStruct(object):

    PARAM_KEY_P_STD = 'std-power'

    def __init__(self, param=None):

        # parameters
        if param is not None:
            if self.PARAM_KEY_P_STD in param:
                self.std_power = param[self.PARAM_KEY_P_STD]

        # state
        self.power = 0

class StorageEntityStruct(object):

    PARAM_KEY_P_MAX_CH = 'max-power-charge'
    PARAM_KEY_P_MAX_DISCH = 'max-power-discharge'
    PARAM_KEY_TOTAL_CAPA = 'total-capacity'

    def __init__(self, param=None):

        # parameters
        self.total_capacity = 2000 # TODO !!
        if param is not None:
            if self.PARAM_KEY_P_MAX_CH in param:
                self.max_power_ch = param[self.PARAM_KEY_P_MAX_CH]
            if self.PARAM_KEY_P_MAX_DISCH in param:
                self.max_power_disch = param[self.PARAM_KEY_P_MAX_DISCH]
            if self.PARAM_KEY_TOTAL_CAPA in param:
                self.total_capacity = param[self.PARAM_KEY_TOTAL_CAPA]

        # state
        self.power = 0
        self.soc = 0
        self.energy = 0

    def getEnergy(self):
        return self.soc * self.total_capacity

    def soc(self, v):
        self.soc = max(0, min(1, v))
